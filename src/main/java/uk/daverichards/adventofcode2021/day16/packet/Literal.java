package uk.daverichards.adventofcode2021.day16.packet;

import lombok.Data;

@Data
public class Literal implements Packet {
    private final Header header;
    private final long value;

    @Override
    public int getVersionSum() {
        return header.getVersion();
    }
}
