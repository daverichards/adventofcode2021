package uk.daverichards.adventofcode2021.day11;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import uk.daverichards.adventofcode2021.spatial.Map2d;
import uk.daverichards.adventofcode2021.spatial.Point2d;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
@RequiredArgsConstructor
public class OctopusSimulator {
    private static final int FLASH_POINT = 9;

    private final Map2d<Integer> map;
    private int flashCount = 0;

    public void step() {
        map.getPoints().forEach((point, val) -> map.setPoint(point, val + 1));
        resolveFlashes();
    }

    private void resolveFlashes() {
        final List<Point2d> hasFlashed = new ArrayList<>();

        for (int i = 0; i < map.getPoints().size(); i++) {
            List<Point2d> toFlash = map.getPoints().entrySet().stream().filter(e -> e.getValue() > FLASH_POINT && !hasFlashed.contains(e.getKey())).map(Map.Entry::getKey).collect(Collectors.toList());

            if (toFlash.isEmpty()) {
                break;
            }

            toFlash.forEach(p -> {
                map.getNeighboursIncludingDiagonals(p).forEach((point, val) -> map.setPoint(point, val + 1));
                hasFlashed.add(p);
            });
        }

        hasFlashed.forEach(p -> map.setPoint(p, 0));

        flashCount += hasFlashed.size();
    }
}
