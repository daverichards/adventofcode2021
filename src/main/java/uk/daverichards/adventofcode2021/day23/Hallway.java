package uk.daverichards.adventofcode2021.day23;

import lombok.Data;

@Data
public class Hallway implements Space {
    private final boolean canBeOccupied;

    @Override
    public boolean isHallway() {
        return true;
    }

    @Override
    public boolean isRoom() {
        return false;
    }

    @Override
    public boolean canBeOccupiedBy(final Amphipod amphipod) {
        return canBeOccupied;
    }
}
