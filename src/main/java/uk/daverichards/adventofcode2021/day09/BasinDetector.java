package uk.daverichards.adventofcode2021.day09;

import uk.daverichards.adventofcode2021.spatial.Map2d;
import uk.daverichards.adventofcode2021.spatial.Point2d;
import uk.daverichards.adventofcode2021.spatial.Vector2d;

import java.util.ArrayList;
import java.util.List;

public class BasinDetector<T> {
    private final Map2d<T> sourceMap;
    private final T boundary;

    private final List<Map2d<Boolean>> basins = new ArrayList<>();
    private final List<Vector2d> pathDirections = List.of(
            new Vector2d(-1, 0),
            new Vector2d(1, 0),
            new Vector2d(0, -1),
            new Vector2d(0, 1)
    );

    public BasinDetector(Map2d<T> sourceMap, T boundary) {
        this.sourceMap = sourceMap;
        this.boundary = boundary;
    }

    private void detectBasins() {
        sourceMap.getPoints().forEach((mapPoint, mapValue) -> {
            if (mapValue != boundary && !pointExistsInBasin(mapPoint)) {
                // Start a basin
                Map2d<Boolean> newBasin = getBasin(mapPoint);
                if (!newBasin.getPoints().isEmpty()) {
                    basins.add(newBasin);
                }
            }
        });
    }

    private boolean pointExistsInBasin(Point2d p) {
        return basins.stream().anyMatch(b -> b.pointExists(p));
    }

    private Map2d<Boolean> getBasin(Point2d startingPoint) {
        final Map2d<Boolean> basin = new Map2d<>();
        addPointToBasinRecursively(basin, startingPoint);
        return basin;
    }

    private void addPointToBasinRecursively(Map2d<Boolean> b, Point2d p) {
        if (!sourceMap.pointExists(p) || sourceMap.getPoint(p) == boundary || b.pointExists(p)) {
            return;
        }
        b.setPoint(p, true);
        pathDirections.forEach(v -> addPointToBasinRecursively(b, p.add(v)));
    }

    public List<Map2d<Boolean>> getBasins() {
        if (basins.isEmpty()) {
            detectBasins();
        }
        return basins;
    }
}
