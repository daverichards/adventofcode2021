package uk.daverichards.adventofcode2021.day03;

import uk.daverichards.adventofcode2021.util.FileReader;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Day03 {
    public static void main(String[] args) {
        try {
            System.out.println("Day 03");
            List<String> input = FileReader.loadStringList("input/day03.txt");

            int byteSize = input.get(0).length();

            StringBuilder gammaRateBinary = new StringBuilder();
            StringBuilder epsilonRateBinary = new StringBuilder();

            for (int i = 0; i < byteSize; i++) {
                if (mostCommonBit(getBits(input, i))) {
                    gammaRateBinary.append("1");
                    epsilonRateBinary.append("0");
                } else {
                    gammaRateBinary.append("0");
                    epsilonRateBinary.append("1");
                }
            }

            int gammaRate = Integer.parseInt(gammaRateBinary.toString(), 2);
            int epsilonRate = Integer.parseInt(epsilonRateBinary.toString(), 2);

            System.out.println("Part 1:");
            System.out.printf("Gamma Rate: %d * Epsilon Rate: %d = %d\n\n", gammaRate, epsilonRate, gammaRate * epsilonRate);

            String oxGenRateBinary = getLifeSupportRating(input, true);
            String scrubberRateBinary = getLifeSupportRating(input, false);

            int oxGenRate = Integer.parseInt(oxGenRateBinary, 2);
            int scrubberRate = Integer.parseInt(scrubberRateBinary, 2);

            System.out.println("Part 2:");
            System.out.printf("OxGen Rate: %d * Scrubber Rate: %d = %d\n\n", oxGenRate, scrubberRate, oxGenRate * scrubberRate);

        } catch (Exception e) {
            System.out.printf("ERROR: %s\n", e.getMessage());
        }
    }

    private static String getLifeSupportRating(List<String> bytes, boolean mostCommonBit) {
        int byteSize = bytes.size();
        for (int i = 0; i < byteSize; i++) {
            boolean mcb = mostCommonBit(getBits(bytes, i), true);
            if (!mostCommonBit) {
                mcb = !mcb;
            }
            char bit = mcb ? '1' : '0';

            int index = i;
            bytes = bytes.stream().filter(b -> b.charAt(index) == bit).collect(Collectors.toList());

            if (bytes.size() == 1) {
                return bytes.get(0);
            }
        }

        if (bytes.size() != 1) {
            throw new RuntimeException(String.format("Was expecting only 1 byte left, found %d", bytes.size()));
        }

        return bytes.get(0);
    }

    private static List<Boolean> getBits(List<String> bytes, int index) {
        List<Boolean> bits = new ArrayList<>();
        for (String b : bytes) {
            bits.add(b.charAt(index) == '1');
        }
        return bits;
    }

    private static boolean mostCommonBit(List<Boolean> bits) {
        return mostCommonBit(bits, true);
    }

    private static boolean mostCommonBit(List<Boolean> bits, boolean dflt) {
        long doubleTrueFreq = bits.stream().filter(b -> b).count() * 2;
        if (doubleTrueFreq == bits.size()) {
            return dflt;
        }
        return doubleTrueFreq > bits.size();
    }
}
