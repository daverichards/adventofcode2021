package uk.daverichards.adventofcode2021.day25;

import lombok.Data;
import uk.daverichards.adventofcode2021.spatial.Map2d;
import uk.daverichards.adventofcode2021.spatial.Point2d;
import uk.daverichards.adventofcode2021.spatial.Vector2d;

import java.util.Set;
import java.util.stream.Collectors;

public class SeaCucumberSimulator {
    private final Map2d<SeaCucumber> map;

    private final int mapWidth;
    private final int mapHeight;

    public SeaCucumberSimulator(Map2d<SeaCucumber> map, int mapWidth, int mapHeight) {
        this.map = map.copy();
        this.mapWidth = mapWidth;
        this.mapHeight = mapHeight;
    }

    public Map2d<SeaCucumber> getMap() {
        return map.copy();
    }

    public int simulateUntilStationary() {
        int steps = 0;
        int moved;
        do {
            moved = moveSeaCucumbers();
            steps++;
        } while (moved > 0);
        return steps;
    }

    private int moveSeaCucumbers() {
        // East facing first
        final Set<SeaCucumberMovement> toMoveEast = findSeaCucumbersToMove(SeaCucumber.EAST);
        toMoveEast.forEach(move -> map.movePoint(move.from, move.to));

        // South facing next
        final Set<SeaCucumberMovement> toMoveSouth = findSeaCucumbersToMove(SeaCucumber.SOUTH);
        toMoveSouth.forEach(move -> map.movePoint(move.from, move.to));

        return toMoveEast.size() + toMoveSouth.size();
    }

    private Set<SeaCucumberMovement> findSeaCucumbersToMove(final Vector2d direction) {
        // Find the SeaCucumbers, calculate their movement, and ignore any that can't move
        return map.getPoints().entrySet().stream()
            .filter(e -> e.getValue().getVector() == direction)
            .map(e -> {
                final Point2d target = e.getKey().add(e.getValue().getVector());
                return new SeaCucumberMovement(e.getKey(), wrapPoint(target));
            }).filter(m -> !map.pointExists(m.getTo()))
            .collect(Collectors.toSet());
    }

    private Point2d wrapPoint(final Point2d p) {
        Point2d wrapped = p;
        final Vector2d horizontalWrapper = new Vector2d(-mapWidth, 0);
        while (wrapped.getX() >= mapWidth) {
            wrapped = wrapped.add(horizontalWrapper);
        }
        final Vector2d verticalWrapper = new Vector2d(0, -mapHeight);
        while (wrapped.getY() >= mapHeight) {
            wrapped = wrapped.add(verticalWrapper);
        }
        return wrapped;
    }

    @Data
    class SeaCucumberMovement {
        private final Point2d from;
        private final Point2d to;
    }
}
