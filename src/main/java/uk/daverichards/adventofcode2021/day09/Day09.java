package uk.daverichards.adventofcode2021.day09;

import uk.daverichards.adventofcode2021.spatial.Map2d;
import uk.daverichards.adventofcode2021.spatial.Point2d;
import uk.daverichards.adventofcode2021.util.FileReader;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Day09 {
    public static void main(String[] args) {
        try {
            System.out.println("Day 09");
            List<String> input = FileReader.loadStringList("input/day09.txt");

            final Map2d<Integer> heatmap = new Map2d<>();
            for (int y = 0; y < input.size(); y++) {
                List<Integer> values = input.get(y).chars().map(Character::getNumericValue).boxed().collect(Collectors.toList());
                for (int x = 0; x < values.size(); x++) {
                    heatmap.setPoint(new Point2d(x, y), values.get(x));
                }
            }

            System.out.println("Part 1:");

            Map<Point2d, Integer> lowPoints = heatmap.getPoints().entrySet().stream()
                    .filter(entry -> heatmap.getNeighbours(entry.getKey()).values().stream().allMatch(n -> n > entry.getValue()))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

            int riskLevelOfLowPoints = lowPoints.values().stream().reduce(0, (acc, i) -> acc + i + 1);

            System.out.printf("Risk level of low points is: %d\n\n", riskLevelOfLowPoints);

            System.out.println("Part 2:");
            final BasinDetector<Integer> basinDetector = new BasinDetector<>(heatmap, 9);

            List<Map2d<Boolean>> basins = basinDetector.getBasins();

//            basins.forEach(b -> {
//                System.out.println(b.toString(v -> "X", " "));
//            });

            // Get the largest 3
            List<Integer> basinSizes = basins.stream().map(b -> b.getPoints().size()).collect(Collectors.toList());
            while (basinSizes.size() > 3) {
                basinSizes.remove(Collections.min(basinSizes));
            }

            System.out.printf("The multiplication of the 3 largest basins %s = %d", basinSizes, basinSizes.stream().reduce(1, (acc, i) -> acc * i));

        } catch (Exception e) {
            System.out.printf("ERROR: %s\n", e.getMessage());
            e.printStackTrace();
        }
    }
}
