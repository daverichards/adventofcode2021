package uk.daverichards.adventofcode2021.spatial;

import lombok.Data;

@Data
public class Vector2d {
    public final Integer x;
    public final Integer y;

    public Vector2d add(Vector2d v) {
        return new Vector2d(x + v.x, y + v.y);
    }

    public Vector2d multiply(int factor) {
        return new Vector2d(x * factor, y * factor);
    }

    public int getManhattanDistance() {
        return Math.abs(x) + Math.abs(y);
    }
}
