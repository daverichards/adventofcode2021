package uk.daverichards.adventofcode2021.day24.modelnumber;

import java.util.LinkedList;
import java.util.List;

public interface InputFunction {
    List<Long> getInputs(final int index, final LinkedList<Long> z);
}
