package uk.daverichards.adventofcode2021.day23;

import lombok.Data;

@Data
public class Room implements Space {
    private final char type;

    @Override
    public boolean isHallway() {
        return false;
    }

    @Override
    public boolean isRoom() {
        return true;
    }

    @Override
    public boolean canBeOccupiedBy(final Amphipod amphipod) {
        return type == amphipod.getType();
    }
}
