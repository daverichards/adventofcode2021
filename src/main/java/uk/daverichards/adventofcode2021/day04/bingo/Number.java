package uk.daverichards.adventofcode2021.day04.bingo;

import lombok.Data;

@Data
public class Number {
    private final int number;
    private boolean called = false;
}
