package uk.daverichards.adventofcode2021.spatial;

import lombok.Data;

@Data
public class Vector3d {
    final int x;
    final int y;
    final int z;

    public Vector3d add(final Vector3d v) {
        return new Vector3d(x + v.x, y + v.y, z + v.z);
    }

    public Vector3d opposite() {
        return new Vector3d(x * -1, y * -1, z * -1);
    }

    public int manhattanMagnitude() {
        return Math.abs(x) + Math.abs(y) + Math.abs(z);
    }

    public Vector3d to(final Vector3d v) {
        return new Vector3d(v.x - x, v.y - y, v.z - z);
    }

    @Override
    public String toString() {
        return "Vector3d[" + x + "," + y + "," + z + "]";
    }
}
