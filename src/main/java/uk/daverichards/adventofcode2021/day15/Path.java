package uk.daverichards.adventofcode2021.day15;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import uk.daverichards.adventofcode2021.spatial.Point2d;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class Path {
    private final List<Point2d> path;
    @Getter private final int pathScore;

    public Path addPoint(final Point2d point, final int value) {
        final List<Point2d> newPath = new ArrayList<>(path);
        newPath.add(point);
        return new Path(newPath, pathScore + value);
    }

    public Point2d getLastPoint() {
        return path.get(path.size() - 1);
    }

    public boolean contains(final Point2d point) {
        return path.contains(point);
    }
}
