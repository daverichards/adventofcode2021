package uk.daverichards.adventofcode2021.day24;

import uk.daverichards.adventofcode2021.day24.alu.ALU;
import uk.daverichards.adventofcode2021.day24.alu.InputBuffer;
import uk.daverichards.adventofcode2021.day24.alu.Program;
import uk.daverichards.adventofcode2021.day24.alu.State;
import uk.daverichards.adventofcode2021.day24.modelnumber.ModelNumberSolver;
import uk.daverichards.adventofcode2021.util.FileReader;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day24 {
  public static void main(String[] args) {
    try {
      System.out.println("Day 24");
      final List<String> input = FileReader.loadStringList("input/day24.txt");

      final Program program = new Program(input);
      final ModelNumberSolver modelNumberSolver = new ModelNumberSolver();

      System.out.println("Part 1:");

      final long largestModelNumber = modelNumberSolver.findLargestModelNumber();
      System.out.format("Largest valid model number: %d\n", largestModelNumber);

      final InputBuffer largestIn =
          new InputBuffer(stringToList(Long.toString(largestModelNumber)));
      final State largestResult = ALU.run(program, largestIn);
      System.out.format("Verified result: %s\n\n", largestResult);

      System.out.println("Part 2:");

      final long smallestModelNumber = modelNumberSolver.findSmallestModelNumber();
      System.out.format("Smallest valid model number: %d\n", smallestModelNumber);

      final InputBuffer smallestIn =
          new InputBuffer(stringToList(Long.toString(smallestModelNumber)));
      final State smallestResult = ALU.run(program, smallestIn);
      System.out.format("Verified result: %s\n\n", smallestResult);
    } catch (Exception e) {
      System.out.printf("ERROR: %s\n", e.getMessage());
      e.printStackTrace();
    }
  }

  private static List<Long> stringToList(final String str) {
    return Arrays.stream(str.split("")).map(Long::parseLong).collect(Collectors.toList());
  }
}
