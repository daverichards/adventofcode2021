package uk.daverichards.adventofcode2021.day15;

import uk.daverichards.adventofcode2021.spatial.Map2d;
import uk.daverichards.adventofcode2021.spatial.Point2d;
import uk.daverichards.adventofcode2021.spatial.Vector2d;
import uk.daverichards.adventofcode2021.util.FileReader;
import uk.daverichards.adventofcode2021.util.Utils;

import java.util.List;

public class Day15 {
    public static void main(String[] args) {
        try {
            System.out.println("Day 15");
            List<String> input = FileReader.loadStringList("input/day15.txt");

            final Map2d<Integer> riskMap = new Map2d<>();
            for (int y = 0; y < input.size(); y++) {
                List<Integer> values = Utils.toIntegers(input.get(y));
                for (int x = 0; x < values.size(); x++) {
                    riskMap.setPoint(new Point2d(x, y), values.get(x));
                }
            }


            System.out.println("Part 1:");
            final PathFinder pathFinder = new PathFinder(riskMap.copy());
            Path lowestRiskPath = pathFinder.findLowestRiskPath(riskMap.getMinBound(), riskMap.getMaxBound());
            System.out.printf("Score of lowest risk path: %d\n\n", lowestRiskPath.getPathScore());

            System.out.println("Part 2:");
            // Generate bigger map
            final Map2d<Integer> bigRiskMap = new Map2d<>();
            Vector2d riskMapTileSize = riskMap.getMinBound().vectorTo(riskMap.getMaxBound()).add(new Vector2d(1, 1));
            riskMap.getPoints().forEach((p, v) -> {
                for (int translateX = 0; translateX < 5; translateX++) {
                    for (int translateY = 0; translateY < 5; translateY++) {
                        final Point2d newPoint = p.add(new Vector2d(riskMapTileSize.x * translateX, riskMapTileSize.y * translateY));
                        final int newValue = v + translateX + translateY;
                        bigRiskMap.setPoint(newPoint, newValue > 9 ? newValue - 9 : newValue);
                    }
                }
            });

            // Find path
            final PathFinder pathFinderBigMap = new PathFinder(bigRiskMap.copy());
            Path lowestRiskPathBigMap = pathFinderBigMap.findLowestRiskPath(bigRiskMap.getMinBound(), bigRiskMap.getMaxBound());
            System.out.printf("Score of lowest risk path: %d\n\n", lowestRiskPathBigMap.getPathScore());

        } catch (Exception e) {
            System.out.printf("ERROR: %s\n", e.getMessage());
            e.printStackTrace();
        }
    }
}
