package uk.daverichards.adventofcode2021.day22;

import uk.daverichards.adventofcode2021.spatial.Box3d;
import uk.daverichards.adventofcode2021.spatial.Map3d;
import uk.daverichards.adventofcode2021.spatial.Point3d;
import uk.daverichards.adventofcode2021.util.FileReader;

import java.util.List;
import java.util.stream.Collectors;

public class Day22 {
  public static void main(String[] args) {
      try {
          System.out.println("Day 22");
          final List<String> input = FileReader.loadStringList("input/day22.txt");

          final List<Instruction> instructions = input.stream().map(Day22::instructionFromString).collect(Collectors.toList());

          System.out.println("Part 1:");
          final Map3d<Boolean> grid = new Map3d<>();
          instructions.stream().filter(Day22::isInitInstruction).forEach(i -> applyInstruction(grid, i));
          final Box3d initRange = new Box3d(new Point3d(-50, -50, -50), new Point3d(50, 50, 50));
          final long onAfterInit = grid.getPoints().entrySet().stream().filter(e -> initRange.contains(e.getKey()) && e.getValue()).count();
          System.out.format("Number of cubes on after init: %d\n\n", onAfterInit);

          System.out.println("Part 2:");
          final InstructionProcessor instructionProcessor = new InstructionProcessor();
          instructions.forEach(instructionProcessor::process);
          System.out.format("Number of cubes on: %d\n\n", instructionProcessor.on());

      } catch (Exception e) {
          System.out.printf("ERROR: %s\n", e.getMessage());
          e.printStackTrace();
      }
  }

  private static Instruction instructionFromString(final String input) {
      final String[] parts = input.split(" ", 2);
      final String[] rangeParts = parts[1].split(",", 3);
      final String[] xParts = rangeParts[0].substring(2).split("\\.\\.", 2);
      final String[] yParts = rangeParts[1].substring(2).split("\\.\\.", 2);
      final String[] zParts = rangeParts[2].substring(2).split("\\.\\.", 2);
      return new Instruction(
          "on".equals(parts[0]),
          new Box3d(
              new Point3d(Integer.parseInt(xParts[0]), Integer.parseInt(yParts[0]), Integer.parseInt(zParts[0])),
              new Point3d(Integer.parseInt(xParts[1]), Integer.parseInt(yParts[1]), Integer.parseInt(zParts[1]))
          )
      );
  }

  private static void applyInstruction(final Map3d<Boolean> map, final Instruction instruction) {
      instruction.getBox().getPoints().forEach(p -> map.setPoint(p, instruction.isOn()));
  }

  private static boolean isInitInstruction(final Instruction instruction) {
      final int initMin = -50;
      final int initMax = 50;
      final Point3d min = instruction.getBox().getMin();
      final Point3d max = instruction.getBox().getMax();
      return min.getX() <= initMax && min.getY() <= initMax && min.getZ() <= initMax &&
          max.getX() >= initMin && max.getY() >= initMin && max.getZ() >= initMin;
  }
}
