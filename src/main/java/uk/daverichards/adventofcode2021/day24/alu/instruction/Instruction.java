package uk.daverichards.adventofcode2021.day24.alu.instruction;

import uk.daverichards.adventofcode2021.day24.alu.InputBuffer;
import uk.daverichards.adventofcode2021.day24.alu.State;

public interface Instruction {
    State execute(final State state, final InputBuffer inputBuffer);
}
