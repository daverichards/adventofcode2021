package uk.daverichards.adventofcode2021.day16.packet;

import lombok.Data;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class Operator implements Packet {
    private final Header header;
    private final List<Packet> packets;

    @Override
    public int getVersionSum() {
        int sum = header.getVersion();
        for (Packet p : packets) {
            sum += p.getVersionSum();
        }
        return sum;
    }

    @Override
    public long getValue() {
        if (header.getTypeId() == 0) {
            return packets.stream().map(Packet::getValue).reduce(0L, Long::sum);
        }
        if (header.getTypeId() == 1) {
            return packets.stream().map(Packet::getValue).reduce(1L, (acc, l) -> acc * l);
        }
        if (header.getTypeId() == 2) {
            return Collections.min(packets.stream().map(Packet::getValue).collect(Collectors.toList()));
        }
        if (header.getTypeId() == 3) {
            return Collections.max(packets.stream().map(Packet::getValue).collect(Collectors.toList()));
        }
        if (header.getTypeId() == 5) {
            return packets.get(0).getValue() > packets.get(1).getValue() ? 1 : 0;
        }
        if (header.getTypeId() == 6) {
            return packets.get(0).getValue() < packets.get(1).getValue() ? 1 : 0;
        }
        if (header.getTypeId() == 7) {
            return packets.get(0).getValue() == packets.get(1).getValue() ? 1 : 0;
        }
        throw new RuntimeException(String.format("Unexpected type id: %d", header.getTypeId()));
    }
}
