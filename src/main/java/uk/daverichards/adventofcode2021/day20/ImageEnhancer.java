package uk.daverichards.adventofcode2021.day20;

import lombok.Data;
import uk.daverichards.adventofcode2021.spatial.Box2d;
import uk.daverichards.adventofcode2021.spatial.Map2d;
import uk.daverichards.adventofcode2021.spatial.Point2d;
import uk.daverichards.adventofcode2021.spatial.Vector2d;

import java.util.List;

@Data
public class ImageEnhancer {
    private static final char LIT_PIXEL = '#';

    private final Algorithm algorithm;

    // Y-axis -> downwards
    private List<Vector2d> inputPixelVectors = List.of(
        new Vector2d(-1, -1),
        new Vector2d(0, -1),
        new Vector2d(1, -1),
        new Vector2d(-1, 0),
        new Vector2d(0, 0),
        new Vector2d(1, 0),
        new Vector2d(-1, 1),
        new Vector2d(0, 1),
        new Vector2d(1, 1)
    );

    public Map2d<Boolean> enhanceImage(final Map2d<Boolean> sourceImage, final int passes) {
        return enhanceImage(sourceImage, passes, false);
    }

    public Map2d<Boolean> enhanceImage(final Map2d<Boolean> sourceImage, final int passes, boolean outOfBoundsPixel) {
        Map2d<Boolean> enhancedImage = sourceImage.copy();
        for (int i = 0; i < passes; i++) {
            enhancedImage = enhanceImage(enhancedImage, outOfBoundsPixel);
            outOfBoundsPixel = algorithm.enhance(outOfBoundsPixel ? "111111111" : "000000000") == LIT_PIXEL;
        }
        return enhancedImage;
    }

    private Map2d<Boolean> enhanceImage(final Map2d<Boolean> sourceImage, final boolean defaultPixel) {
        final Map2d<Boolean> enhancedImage = new Map2d<>();
        final Point2d enhancedMinBound = sourceImage.getMinBound().add(new Vector2d(-1, -1));
        final Point2d enhancedMaxBound = sourceImage.getMaxBound().add(new Vector2d(1, 1));
        new Box2d(enhancedMinBound, enhancedMaxBound).getPoints().forEach(p -> enhancedImage.setPoint(p, sourcePixelToEnhancedPixel(sourceImage, p, defaultPixel)));
        return enhancedImage;
    }

    private String sourcePixelToBinary(final Map2d<Boolean> sourceImage, final Point2d pixel, final boolean defaultPixel) {
        final StringBuilder sb = new StringBuilder();
        inputPixelVectors.forEach(
            v -> sb.append(
                Boolean.TRUE.equals(sourceImage.getPointOrDefault(pixel.add(v), defaultPixel)) ? "1" : "0"));
        return sb.toString();
    }

    private boolean sourcePixelToEnhancedPixel(final Map2d<Boolean> sourceImage, final Point2d pixel, final boolean defaultPixel) {
        return algorithm.enhance(sourcePixelToBinary(sourceImage, pixel, defaultPixel)) == LIT_PIXEL;
    }
}
