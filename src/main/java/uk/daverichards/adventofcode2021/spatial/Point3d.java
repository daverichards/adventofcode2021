package uk.daverichards.adventofcode2021.spatial;

import lombok.Data;

@Data
public class Point3d {
    final int x;
    final int y;
    final int z;

    public Point3d add(final Vector3d v) {
        return new Point3d(x + v.x, y + v.y, z + v.z);
    }

    public Vector3d to(final Point3d v) {
        return new Vector3d(v.x - x, v.y - y, v.z - z);
    }

    public Vector3d toVector() {
        return new Vector3d(x, y, z);
    }

    @Override
    public String toString() {
        return "Point3d[" + x + "," + y + "," + z + "]";
    }
}
