package uk.daverichards.adventofcode2021.day16;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PacketStringReader {
    private String packetString;

    public int length() {
        return packetString.length();
    }

    public boolean isEmpty() {
        return packetString.isEmpty();
    }

    public String pop(final int length) {
        final String popped = packetString.substring(0, length);
        packetString = packetString.substring(length);
        return popped;
    }
}
