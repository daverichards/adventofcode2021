package uk.daverichards.adventofcode2021.day23;

import lombok.Getter;
import uk.daverichards.adventofcode2021.spatial.Box2d;
import uk.daverichards.adventofcode2021.spatial.Map2d;
import uk.daverichards.adventofcode2021.spatial.Path2d;
import uk.daverichards.adventofcode2021.spatial.Point2d;
import uk.daverichards.adventofcode2021.spatial.Vector2d;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class AmphipodOrganiser {
    private final int roomSize;
    private final Map2d<Space> map = new Map2d<>();
    private final List<Amphipod> startingAmphipods = new ArrayList<>();
    private final Map<State, Integer> minimumStepsToCompleteCache = new HashMap<>();
    private final Map<Integer, Integer> triangularNumbers = new HashMap<>();

    @Getter
    private final List<Character> rooms = List.of('A', 'B', 'C', 'D');

    public AmphipodOrganiser(final int roomSize) {
        this.roomSize = roomSize;
        for (int i = 0; i <= roomSize; i++) {
            triangularNumbers.put(i, calculateTriangularNumber(i));
        }
        buildMap();
    }

    private void buildMap() {
        // Hallway
        map.setPoints(new Box2d(new Point2d(0, 0), new Point2d(10, 0)), new Hallway(true));
        // Rooms
        int x = 2;
        for (char room : rooms) {
            map.setPoint(new Point2d(x, 0), new Hallway(false));
            for (int y = 1; y <= roomSize; y++) {
                map.setPoint(new Point2d(x, y), new Room(room));
            }
            x += 2;
        }
    }

    public void addAmphipod(final Amphipod amphipod) {
        if (!map.pointExists(amphipod.getLocation())) {
            throw new IllegalArgumentException("Trying to place amphipod out of bounds: " + amphipod.getLocation());
        }
        startingAmphipods.add(amphipod);
    }

    public State getStaringState() {
        return new State(startingAmphipods);
    }

    public Path findShortestPath() {
        final Optional<Path> shortestPath = findCompletePaths().stream().reduce((acc, p) -> acc.getTotalEnergySpent() < p.getTotalEnergySpent() ? acc : p);
        if (shortestPath.isEmpty()) {
            return null;
        }
        return shortestPath.get();
    }

    public Set<Path> findCompletePaths() {
        final Set<Path> completePaths = new HashSet<>();
        Optional<Integer> shortestCompletePath = Optional.empty();
        final Set<Path> paths = new HashSet<>();
        paths.add(new Path(getStaringState(), 0));

        final Map<State, Integer> shortestEnergyToStates = new HashMap<>();

        while (true) {
            // Find non-complete Path
            final Optional<Path> path = paths.stream().reduce((a, b) -> getPathCompletionScore(a) < getPathCompletionScore(b) ? a : b);

            if (path.isEmpty()) {
                // No more non-complete paths
                return completePaths;
            }

            // Remove this path
            paths.remove(path.get());

            // Get next paths
            final Set<Path> nextPaths = nextPaths(path.get());

            if (nextPaths.isEmpty()) {
                continue;
            }

            // Filter paths
            if (nextPaths.stream().anyMatch(this::isComplete)) {
                // Complete paths
                completePaths.addAll(nextPaths.stream().filter(this::isComplete).collect(Collectors.toSet()));
                shortestCompletePath = completePaths.stream().map(Path::getTotalEnergySpent).reduce((a, b) -> a < b ? a : b);
                shortestCompletePath.ifPresent(shortest -> paths.removeIf(p -> getPathCompletionScore(p) >= shortest));
                nextPaths.removeIf(this::isComplete);
            }

            shortestCompletePath.ifPresent(shortest -> nextPaths.removeIf(p -> getPathCompletionScore(p) >= shortest));

            nextPaths.removeIf(p -> p.getSteps().stream().anyMatch(s -> shortestEnergyToStates.containsKey(s) && shortestEnergyToStates.get(s) < p.getEnergySpentAtStep(s)));

            if (nextPaths.isEmpty()) {
                continue;
            }

            final Map<State, Integer> newShortestEnergyToStates = new HashMap<>();
            nextPaths.forEach(p -> newShortestEnergyToStates.put(p.getLastStep(), p.getTotalEnergySpent()));

            paths.removeIf(p -> p.getSteps().stream().anyMatch(s -> newShortestEnergyToStates.containsKey(s) && newShortestEnergyToStates.get(s) <= p.getEnergySpentAtStep((s))));

            shortestEnergyToStates.putAll(newShortestEnergyToStates);

            // Combine next paths
            paths.addAll(nextPaths);
        }
    }

    private Set<Path> nextPaths(final Path path) {
        final Set<Path> nextPaths = new HashSet<>();
        final State currentState = path.getLastStep();
        currentState.getAmphipods().forEach(a -> {
            // What are the possible next steps for this Amphipod?
            if (isRoomSpaceLocked(a.getLocation(), currentState)) {
                // Don't move if they are in their room and not blocking one anyone
                return;
            }
            possibleNextLocations(a, currentState).forEach(p -> {
                final State nextState = currentState.moveAmphipodTo(a, p);
                if (!path.contains(nextState)) {
                    nextPaths.add(path.addStep(
                        nextState,
                        a.getLocation().vectorTo(p).getManhattanDistance() * a.getEnergyCost()
                    ));
                }
            });
        });
        return nextPaths;
    }

    private Set<Point2d> possibleNextLocations(final Amphipod amphipod, final State state) {
        final Set<Point2d> possibleNextLocations = new HashSet<>();
        final Space currentSpace = map.getPoint(amphipod.getLocation());
        final Set<Path2d> paths = new HashSet<>(Set.of(new Path2d(List.of(amphipod.getLocation()))));

        while (!paths.isEmpty()) {
            for (Path2d path : new HashSet<>(paths)) {
                paths.remove(path);
                map.getNeighbours(path.getLastPoint()).forEach((p, s) -> {
                    if (state.isAmphipodFoundAt(p) || path.contains(p)) {
                        // Blocked or been here before
                        return;
                    }
                    if (s.canBeOccupiedBy(amphipod)
                        && currentSpace.isRoom() != s.isRoom()
                        && (!s.isRoom() || isRoomSpaceOpen(p, state))
                    ) {
                        // Can move here
                        possibleNextLocations.add(p);
                    }
                    // Add as a visit
                    paths.add(path.addPoint(p));
                });
            }
        }
        return possibleNextLocations;
    }

    private boolean isRoomSpaceOpen(final Point2d p, final State state) {
        final Space space = map.getPoint(p);
        final Point2d nextRoomSpace = p.add(new Vector2d(0, 1));
        return space.isRoom() && !state.isAmphipodFoundAt(p) && isRoomSpaceLocked(nextRoomSpace, state);
    }

    private boolean isRoomSpaceLocked(final Point2d p, final State state) {
        final Space space = map.getPoint(p);
        if (space == null) {
            return true;
        }
        if (!space.isRoom() || !state.isAmphipodFoundAt(p)) {
            return false;
        }
        final Amphipod amphipod = state.getAmphipodFoundAt(p);
        final Point2d nextRoomSpace = p.add(new Vector2d(0, 1));
        return space.canBeOccupiedBy(amphipod) && isRoomSpaceLocked(nextRoomSpace, state);
    }

    private int getPathCompletionScore(final Path path) {
        return path.getTotalEnergySpent() + getCachedStateMinimumStepsToComplete(path.getLastStep());
    }

    private int getCachedStateMinimumStepsToComplete(final State state) {
        if (!minimumStepsToCompleteCache.containsKey(state)) {
            minimumStepsToCompleteCache.put(state, getStateMinimumStepsToComplete(state));
        }
        return minimumStepsToCompleteCache.get(state);
    }

    private int getStateMinimumStepsToComplete(final State state) {
        final Map<Character, Integer> roomXCoords = Map.of('A', 2, 'B', 4, 'C', 6, 'D', 8);
        final Map<Character, Integer> correctRooms = new HashMap<>();
        int minRemainingSteps = 0;
        for (Amphipod a : state.getAmphipods()) {
            final Space space = map.getPoint(a.getLocation());
            if (space.isRoom() && isRoomSpaceLocked(a.getLocation(), state)) {
                correctRooms.put(a.getType(), correctRooms.getOrDefault(a.getType(), 0) + 1);
                continue;
            }
            // Steps to entrance of room
            minRemainingSteps +=
                (Objects.equals(a.getLocation().getX(), roomXCoords.get(a.getType())) ? 2 : Math.abs(a.getLocation().getX() - roomXCoords.get(a.getType())))
                    + a.getLocation().getY();
        }
        // Steps from entrance to place
        for (char room : rooms) {
            minRemainingSteps += triangularNumbers.get(roomSize - correctRooms.getOrDefault(room, 0));
        }
        return minRemainingSteps;
    }

    private int calculateTriangularNumber(int i) {
        int sum = 0;
        while (i > 0) {
            sum += i--;
        }
        return sum;
    }

    private boolean isComplete(final Path path) {
        return isComplete(path.getLastStep());
    }

    private boolean isComplete(final State state) {
        for (Amphipod a : state.getAmphipods()) {
            final Space space = map.getPoint(a.getLocation());
            if (!space.isRoom() || !space.canBeOccupiedBy(a)) {
                return false;
            }
        }
        return true;
    }

    public String toString(final Path path) {
        final StringBuilder sb = new StringBuilder();
        for (State state : path.getSteps()) {
            sb.append(toString(state));
            sb.append("\n");
        }
        return sb.toString();
    }

    public String toString(final State state) {
        final Map2d<String> output = new Map2d<>();
        output.setPoints(new Box2d(new Point2d(-1, -1), new Point2d(11, roomSize + 1)), "#");
        output.setPoints(map.getPoints().keySet(), ".");
        state.getAmphipods().forEach(a -> output.setPoint(a.getLocation(), String.valueOf(a.getType())));
        return output.toString(s -> s, " ");
    }
}
