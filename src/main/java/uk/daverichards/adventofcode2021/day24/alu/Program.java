package uk.daverichards.adventofcode2021.day24.alu;

import uk.daverichards.adventofcode2021.day24.alu.instruction.Add;
import uk.daverichards.adventofcode2021.day24.alu.instruction.Divide;
import uk.daverichards.adventofcode2021.day24.alu.instruction.Equal;
import uk.daverichards.adventofcode2021.day24.alu.instruction.Input;
import uk.daverichards.adventofcode2021.day24.alu.instruction.Instruction;
import uk.daverichards.adventofcode2021.day24.alu.instruction.Modulo;
import uk.daverichards.adventofcode2021.day24.alu.instruction.Multiply;

import java.util.ArrayList;
import java.util.List;

public class Program {
    private final List<Instruction> instructions = new ArrayList<>();

    public Program(final List<String> input) {
        input.forEach(i -> instructions.add(parseInstruction(i)));
    }

    public List<Instruction> getInstructions() {
        return new ArrayList<>(instructions);
    }

    private Instruction parseInstruction(final String input) {
        final String[] parts = input.split(" +");
        if ("inp".equals(parts[0])) {
            return new Input(parts[1]);
        }
        if ("add".equals(parts[0])) {
            if (isStateAddr(parts[2])) {
                return new Add(parts[1], parts[2]);
            }
            return new Add(parts[1], Long.parseLong(parts[2]));
        }
        if ("mul".equals(parts[0])) {
            if (isStateAddr(parts[2])) {
                return new Multiply(parts[1], parts[2]);
            }
            return new Multiply(parts[1], Long.parseLong(parts[2]));
        }
        if ("div".equals(parts[0])) {
            if (isStateAddr(parts[2])) {
                return new Divide(parts[1], parts[2]);
            }
            return new Divide(parts[1], Long.parseLong(parts[2]));
        }
        if ("mod".equals(parts[0])) {
            if (isStateAddr(parts[2])) {
                return new Modulo(parts[1], parts[2]);
            }
            return new Modulo(parts[1], Long.parseLong(parts[2]));
        }
        if ("eql".equals(parts[0])) {
            if (isStateAddr(parts[2])) {
                return new Equal(parts[1], parts[2]);
            }
            return new Equal(parts[1], Long.parseLong(parts[2]));
        }

        throw new RuntimeException(String.format("Cannot parse instruction: '%s'", input));
    }

    private boolean isStateAddr(final String input) {
        return input.matches("^[a-z]+$");
    }
}
