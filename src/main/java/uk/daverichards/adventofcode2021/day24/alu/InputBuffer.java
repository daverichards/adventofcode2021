package uk.daverichards.adventofcode2021.day24.alu;

import java.util.LinkedList;
import java.util.List;

public class InputBuffer {
    private final LinkedList<Long> buffer;

    public InputBuffer(List<Long> buffer) {
        this.buffer = new LinkedList<>(buffer);
    }

    public long read() {
        if (buffer.isEmpty()) {
            throw new RuntimeException("Cannot read from InputBuffer: empty");
        }
        final long next = buffer.getFirst();
        buffer.removeFirst();
        return next;
    }
}
