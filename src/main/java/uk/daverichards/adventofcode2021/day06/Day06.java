package uk.daverichards.adventofcode2021.day06;

import uk.daverichards.adventofcode2021.util.FileReader;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day06 {
    public static void main(String[] args) {

        try {
            System.out.println("Day 06");
            List<String> input = FileReader.loadStringList("input/day06.txt");
            List<Integer> startFish = Arrays.stream(input.get(0).split(",")).map(Integer::parseInt).collect(Collectors.toList());

            System.out.println("Part 1:");
            final int p1Days = 80;
            LanternfishSimulator simulator = new LanternfishSimulator(6, 8);
            long p1TotalFish = simulator.simulateSchool(startFish, p1Days);
            System.out.printf("After %d days there would be %d fish\n\n", p1Days, p1TotalFish);

            System.out.println("Part 2:");
            final int p2Days = 256;
            long p2TotalFish = simulator.simulateSchool(startFish, p2Days);
            System.out.printf("After %d days there would be %d fish\n\n", p2Days, p2TotalFish);

        } catch (Exception e) {
            System.out.printf("ERROR: %s\n", e.getMessage());
            e.printStackTrace();
        }
    }
}
