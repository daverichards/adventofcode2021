package uk.daverichards.adventofcode2021.day22;

import uk.daverichards.adventofcode2021.spatial.Box3d;
import uk.daverichards.adventofcode2021.spatial.Point3d;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class InstructionProcessor {
    private final Set<Box3d> onBoxes = new HashSet<>();

    public long on() {
        return onBoxes.stream().map(Box3d::size).reduce(0L, Long::sum);
    }

    public void process(final Instruction instruction) {
        if (instruction.isOn()) {
            turnOn(instruction.getBox());
        } else {
            turnOff(instruction.getBox());
        }
    }

    private void turnOn(final Box3d on) {
        // First remove so there is no overlap
        turnOff(on);
        onBoxes.add(on);
    }

    private void turnOff(final Box3d off) {
        final Map<Box3d, Set<Box3d>> replacements = new HashMap<>();
        onBoxes.stream().filter(on -> on.intersects(off)).forEach(on -> replacements.put(on, getSlicedBoxRemains(on, off)));
        if (!replacements.isEmpty()) {
            replacements.forEach((k, v) -> {
                onBoxes.remove(k);
                onBoxes.addAll(v);
            });
        }
    }

    private Set<Box3d> getSlicedBoxRemains(final Box3d on, final Box3d off) {
        final Set<Box3d> remaining = new HashSet<>();
        if (off.getMin().getX() > on.getMin().getX()) {
            remaining.add(new Box3d(
                on.getMin(),
                new Point3d(off.getMin().getX() - 1, on.getMax().getY(), on.getMax().getZ())
            ));
        }
        if (off.getMax().getX() < on.getMax().getX()) {
            remaining.add(new Box3d(
                new Point3d(off.getMax().getX() + 1, on.getMin().getY(), on.getMin().getZ()),
                on.getMax()
            ));
        }
        if (off.getMin().getY() > on.getMin().getY()) {
            remaining.add(new Box3d(
                new Point3d(Math.max(on.getMin().getX(), off.getMin().getX()), on.getMin().getY(), on.getMin().getZ()),
                new Point3d(Math.min(on.getMax().getX(), off.getMax().getX()), off.getMin().getY() - 1, on.getMax().getZ())
            ));
        }
        if (off.getMax().getY() < on.getMax().getY()) {
            remaining.add(new Box3d(
                new Point3d(Math.max(on.getMin().getX(), off.getMin().getX()), off.getMax().getY() + 1, on.getMin().getZ()),
                new Point3d(Math.min(on.getMax().getX(), off.getMax().getX()), on.getMax().getY(), on.getMax().getZ())
            ));
        }
        if (off.getMin().getZ() > on.getMin().getZ()) {
            remaining.add(new Box3d(
                new Point3d(Math.max(on.getMin().getX(), off.getMin().getX()), Math.max(on.getMin().getY(), off.getMin().getY()), on.getMin().getZ()),
                new Point3d(Math.min(on.getMax().getX(), off.getMax().getX()), Math.min(on.getMax().getY(), off.getMax().getY()), off.getMin().getZ() - 1)
            ));
        }
        if (off.getMax().getZ() < on.getMax().getZ()) {
            remaining.add(new Box3d(
                new Point3d(Math.max(on.getMin().getX(), off.getMin().getX()), Math.max(on.getMin().getY(), off.getMin().getY()), off.getMax().getZ() + 1),
                new Point3d(Math.min(on.getMax().getX(), off.getMax().getX()), Math.min(on.getMax().getY(), off.getMax().getY()), on.getMax().getZ())
            ));
        }
        return remaining;
    }
}
