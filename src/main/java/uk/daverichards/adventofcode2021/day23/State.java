package uk.daverichards.adventofcode2021.day23;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import uk.daverichards.adventofcode2021.spatial.Point2d;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
public class State {
    private final List<Amphipod> amphipods;

    public List<Amphipod> getAmphipods() {
        return new ArrayList<>(amphipods);
    }

    public State moveAmphipodTo(final Amphipod a, final Point2d p) {
        final List<Amphipod> amphipods = getAmphipods();
        final int index = amphipods.indexOf(a);
        if (index == -1) {
            throw new IllegalArgumentException("Cannot find Amphipod within state");
        }
        amphipods.set(index, a.moveTo(p));
        return new State(amphipods);
    }

    public boolean isAmphipodFoundAt(final Point2d p) {
        for (Amphipod a : amphipods) {
            if (a.getLocation().equals(p)) {
                return true;
            }
        }
        return false;
    }

    public Amphipod getAmphipodFoundAt(final Point2d p) {
        for (Amphipod a : amphipods) {
            if (a.getLocation().equals(p)) {
                return a;
            }
        }
        return null;
    }
}
