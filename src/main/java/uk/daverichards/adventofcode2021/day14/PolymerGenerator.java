package uk.daverichards.adventofcode2021.day14;

import java.util.HashMap;
import java.util.Map;

public class PolymerGenerator {
    private final Map<Pair, Character> rules = new HashMap<>();
    private final Map<Pair, Map<Integer, Map<Character, Long>>> cache = new HashMap<>();

    public void addRule(final String pair, final String insert) {
        if (insert.length() != 1) {
            throw new RuntimeException(String.format("Invalid rule insert: %s", insert));
        }
        rules.put(Pair.fromString(pair), insert.charAt(0));
    }

    public Map<Character, Long> generateCharFrequenciesFromTemplate(final String template, final int rounds) {
        final Map<Character, Long> charFrequencies = new HashMap<>();

        final char[] templateChars = template.toCharArray();
        for (int i = 0; i < (templateChars.length - 1); i++) {
            // Add char to frequencies
            incrementCharFrequency(charFrequencies, templateChars[i]);
            // Add frequencies of inserted chars
            combineCharFrequencies(
                charFrequencies,
                insertPair(new Pair(templateChars[i], templateChars[i + 1]), rounds)
            );

        }

        // Add last char
        incrementCharFrequency(charFrequencies, templateChars[templateChars.length - 1]);

        return charFrequencies;
    }

    private Map<Character, Long> insertPair(final Pair pair, final int roundsRemaining) {
        if (cacheExists(pair, roundsRemaining)) {
            // Return cached value
            return cacheFetch(pair, roundsRemaining);
        }
        final Map<Character, Long> charFrequencies = new HashMap<>();
        if (rules.containsKey(pair)) {
            // Rule found for this pair
            final Character insert = rules.get(pair);
            // Add char to frequencies
            incrementCharFrequency(charFrequencies, insert);
            if (roundsRemaining > 1) {
                // There are rounds remaining combine the frequencies from the 2 new pairs created by this insert
                combineCharFrequencies(
                    charFrequencies,
                    insertPair(new Pair(pair.a, insert), roundsRemaining - 1)
                );
                combineCharFrequencies(
                    charFrequencies,
                    insertPair(new Pair(insert, pair.b), roundsRemaining - 1)
                );
            }
        }
        // Store in cache
        cacheStore(pair, roundsRemaining, charFrequencies);
        return charFrequencies;
    }

    private void incrementCharFrequency(final Map<Character, Long> f, char c) {
        incrementCharFrequency(f, c, 1);
    }

    private void incrementCharFrequency(final Map<Character, Long> f, char c, long i) {
        f.put(c, f.getOrDefault(c, 0L) + i);
    }

    private void combineCharFrequencies(final Map<Character, Long> a, final Map<Character, Long> b) {
        b.forEach((k, v) -> {
            incrementCharFrequency(a, k, v);
        });
    }

    private boolean cacheExists(final Pair pair, final int roundsRemaining) {
        if (!cache.containsKey(pair)) {
            return false;
        }
        return cache.get(pair).containsKey(roundsRemaining);
    }

    private Map<Character, Long> cacheFetch(final Pair pair, final int roundsRemaining) {
        if (!cache.containsKey(pair)) {
            return null;
        }
        return cache.get(pair).get(roundsRemaining);
    }

    private void cacheStore(final Pair pair, final int roundsRemaining, final Map<Character, Long> charFrequencies) {
        if (!cache.containsKey(pair)) {
            cache.put(pair, new HashMap<>());
        }
        cache.get(pair).put(roundsRemaining, charFrequencies);
    }
}
