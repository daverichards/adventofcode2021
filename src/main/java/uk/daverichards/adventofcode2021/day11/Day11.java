package uk.daverichards.adventofcode2021.day11;

import uk.daverichards.adventofcode2021.spatial.Map2d;
import uk.daverichards.adventofcode2021.spatial.Point2d;
import uk.daverichards.adventofcode2021.util.FileReader;
import uk.daverichards.adventofcode2021.util.Utils;

import java.util.List;

public class Day11 {
    public static void main(String[] args) {
        try {
            System.out.println("Day 11");
            List<String> input = FileReader.loadStringList("input/day11.txt");

            final Map2d<Integer> startingMap = new Map2d<>();
            for (int y = 0; y < input.size(); y++) {
                List<Integer> values = Utils.toIntegers(input.get(y));
                for (int x = 0; x < values.size(); x++) {
                    startingMap.setPoint(new Point2d(x, y), values.get(x));
                }
            }


            System.out.println("Part 1:");
            final OctopusSimulator octopusSimulatorPart1 = new OctopusSimulator(startingMap.copy());
            for (int i = 0; i < 100; i++) {
                octopusSimulatorPart1.step();
            }
            System.out.printf("Number of flashes: %d\n\n", octopusSimulatorPart1.getFlashCount());

            System.out.println("Part 2:");
            final OctopusSimulator octopusSimulatorPart2 = new OctopusSimulator(startingMap.copy());
            int syncCounter = 0;
            while (!octopusSimulatorPart2.getMap().getPoints().values().stream().allMatch(i -> i == 0)) {
                octopusSimulatorPart2.step();
                syncCounter++;
            }
            System.out.printf("First step all flash at the same time: %d\n\n", syncCounter);
        } catch (Exception e) {
            System.out.printf("ERROR: %s\n", e.getMessage());
            e.printStackTrace();
        }

    }
}
