package uk.daverichards.adventofcode2021.day24.alu.instruction;

import lombok.RequiredArgsConstructor;
import uk.daverichards.adventofcode2021.day24.alu.InputBuffer;
import uk.daverichards.adventofcode2021.day24.alu.State;

@RequiredArgsConstructor
public class Input implements Instruction {
    private final String addr;

    @Override
    public State execute(final State state, final InputBuffer inputBuffer) {
        final long inp = inputBuffer.read();
        return state.set(addr, inp);
    }
}
