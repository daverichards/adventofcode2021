package uk.daverichards.adventofcode2021.day06;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LanternfishSimulator {
    private final int adultPeriod;
    private final int newBornPeriod;

    private final Map<Integer, Map<Integer, Long>> cache = new HashMap<>();

    public LanternfishSimulator(int adultPeriod, int newBornPeriod) {
        this.adultPeriod = adultPeriod;
        this.newBornPeriod = newBornPeriod;

        for (int i = 0; i <= Math.max(adultPeriod, newBornPeriod); i++) {
            cache.put(i, new HashMap<>());
        }
    }

    public long simulateSchool(final List<Integer> school, final int roundsRemaining) {
        long result = 0;
        for (int fish : school) {
            result += simulateAndCache(fish, roundsRemaining);
        }
        return result;
    }

    public long simulateAndCache(final int daysToBirth, final int roundsRemaining) {
        if (!cache.get(daysToBirth).containsKey(roundsRemaining)) {
            cache.get(daysToBirth).put(roundsRemaining, simulate(daysToBirth, roundsRemaining));
        }
        return cache.get(daysToBirth).get(roundsRemaining);
    }

    private long simulate(final int daysToBirth, final int roundsRemaining) {
        if (roundsRemaining == 1) {
            return daysToBirth == 0 ? 2 : 1;
        }
        if (daysToBirth == 0) {
            return simulateAndCache(adultPeriod, roundsRemaining - 1) + simulateAndCache(newBornPeriod, roundsRemaining - 1);
        }
        return simulate(daysToBirth - 1, roundsRemaining - 1);
    }
}
