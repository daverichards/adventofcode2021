package uk.daverichards.adventofcode2021.day12;

import uk.daverichards.adventofcode2021.util.FileReader;

import java.util.List;
import java.util.Set;

public class Day12 {
    public static void main(String[] args) {
        try {
            System.out.println("Day 12");
            List<String> input = FileReader.loadStringList("input/day12.txt");

            final PathFinder pathFinder = new PathFinder();
            input.forEach(line -> {
                String[] parts = line.split("-", 2);
                if (parts.length != 2) {
                    throw new RuntimeException(String.format("Invalid path pair: %s", line));
                }
                pathFinder.addTwoWayPath(parts[0], parts[1]);
            });

            System.out.println("Part 1:");
            final Set<List<String>> paths = pathFinder.findPaths();
            // System.out.println(paths);
            System.out.printf("Number of different paths: %d\n\n", paths.size());

            System.out.println("Part 2:");
            final Set<List<String>> pathsWithOneSmallCaveRevisit = pathFinder.findPaths(1);
            // System.out.println(pathsWithOneSmallCaveRevisit);
            System.out.printf("Number of different paths whe 1 small cave can be revisited: %d\n\n", pathsWithOneSmallCaveRevisit.size());
        } catch (Exception e) {
            System.out.printf("ERROR: %s\n", e.getMessage());
            e.printStackTrace();
        }
    }
}
