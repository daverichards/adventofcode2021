package uk.daverichards.adventofcode2021.day05;

import uk.daverichards.adventofcode2021.spatial.Line2d;
import uk.daverichards.adventofcode2021.spatial.Map2d;
import uk.daverichards.adventofcode2021.spatial.Point2d;
import uk.daverichards.adventofcode2021.util.FileReader;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Day05 {
    public static void main(String[] args) {
        try {
            System.out.println("Day 05");
            List<String> input = FileReader.loadStringList("input/day05.txt");

            final List<Line2d> lines = buildLines(input);

            System.out.println("Part 1:");
            final List<Line2d> straightLines = lines.stream().filter(Line2d::isHorizontalOrVertical).collect(Collectors.toList());
            final Map2d<Integer> p1Map = new Map2d<>();

            for (Line2d l : straightLines) {
                for (Point2d p : l.getPoints()) {
                    p1Map.setPoint(p, p1Map.getPointOrDefault(p, 0) + 1);
                }
            }

            System.out.println(p1Map.toString(i -> " " + i.toString(), " 0"));
            long p1OverlappingPoints = p1Map.getPoints().entrySet().stream().filter(entry -> entry.getValue() > 1).count();
            System.out.printf("Number of overlapping points: %d\n\n", p1OverlappingPoints);

            System.out.println("Part 2:");
            final Map2d<Integer> p2Map = new Map2d<>();

            for (Line2d l : lines) {
                for (Point2d p : l.getPoints()) {
                    p2Map.setPoint(p, p2Map.getPointOrDefault(p, 0) + 1);
                }
            }

            System.out.println(p2Map.toString(i -> " " + i.toString(), " 0"));
            long p2OverlappingPoints = p2Map.getPoints().entrySet().stream().filter(entry -> entry.getValue() > 1).count();
            System.out.printf("Number of overlapping points: %d\n\n", p2OverlappingPoints);

        } catch (Exception e) {
            System.out.printf("ERROR: %s\n", e.getMessage());
            e.printStackTrace();
        }
    }

    private static List<Line2d> buildLines(List<String> data) {
        List<Line2d> lines = new ArrayList<>();
        data.forEach(l -> {
            String[] parts = l.split(" -> ");
            lines.add(new Line2d(
                    Point2d.fromString(parts[0]),
                    Point2d.fromString(parts[1])
            ));
        });
        return lines;
    }
}
