package uk.daverichards.adventofcode2021.day04;

import uk.daverichards.adventofcode2021.day04.bingo.Card;
import uk.daverichards.adventofcode2021.day04.bingo.Number;
import uk.daverichards.adventofcode2021.day04.bingo.NumberRepository;
import uk.daverichards.adventofcode2021.util.FileReader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day04 {
    public static void main(String[] args) {
        try {
            System.out.println("Day 04");
            List<String> input = FileReader.loadStringList("input/day04.txt");

            NumberRepository numbers = new NumberRepository(0, 99);
            List<Integer> callOrder = Arrays.stream(input.get(0).split(",")).map(Integer::parseInt).collect(Collectors.toList());

            List<Card> cards = new ArrayList<>();
            Card currentCard = new Card();
            for (int i = 2; i < input.size(); i++) {
                String line = input.get(i);
                if (line.isEmpty()) {
                    cards.add(currentCard);
                    currentCard = new Card();
                    continue;
                }
                // Add row
                currentCard.addRow(
                    Arrays.stream(line.trim().split(" +")).map(n -> numbers.get(Integer.parseInt(n.trim()))).collect(Collectors.toList())
                );
            }
            cards.add(currentCard);

            List<Card> winners = new ArrayList<>();

            for (Integer i : callOrder) {
                numbers.get(i).setCalled(true);

                for (Card card : cards) {
                    if (!winners.contains(card) && card.isWinner()) {
                        winners.add(card);

                        System.out.printf("WINNER!\nCalled number: %d\n", i);
                        List<Integer> uncalledNumbers = card.getUncalledNumbers();
                        int uncalledSum = uncalledNumbers.stream().reduce(0, Integer::sum);
                        System.out.printf("Sum of remaining numbers: %d\n", uncalledSum);
                        System.out.printf("%d * %d = %d\n", i, uncalledSum, i * uncalledSum);
                    }
                }
            }

            System.out.println("Part 1:");
            // First winner

            System.out.println("Part 2:");
            // Last winner

        } catch (Exception e) {
            System.out.printf("ERROR: %s\n", e.getMessage());
            e.printStackTrace();
        }
    }
}
