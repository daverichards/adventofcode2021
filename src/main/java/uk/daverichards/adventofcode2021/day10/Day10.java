package uk.daverichards.adventofcode2021.day10;

import uk.daverichards.adventofcode2021.util.FileReader;
import uk.daverichards.adventofcode2021.util.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Day10 {
    public static void main(String[] args) {
        try {
            System.out.println("Day 10");
            List<String> input = FileReader.loadStringList("input/day10.txt");

            final SyntaxParser parser = new SyntaxParser();

            System.out.println("Part 1:");
            final Map<Character, Integer> illegalCharacterScores = Map.of(
                    ')',3,
                    ']', 57,
                    '}', 1197,
                    '>', 25137
            );
            int syntaxErrorScore = 0;
            final List<List<Character>> autocompletes = new ArrayList<>();
            for (String line : input) {
                try {
                    List<Character> autocomplete = parser.parseLine(Utils.toCharacters(line));
                    if (!autocomplete.isEmpty()) {
                        autocompletes.add(autocomplete);
                    }
                } catch (SyntaxCorruptException e) {
                    // System.out.println(e.getMessage());
                    syntaxErrorScore += illegalCharacterScores.get(e.getIllegalCharacter());
                }
            }
            System.out.printf("Illegal character score: %d\n\n", syntaxErrorScore);

            System.out.println("Part 2:");
            final List<Long> autocompleteScores = autocompletes.stream().map(Day10::calcAutocompleteScore).collect(Collectors.toList());
            while (autocompleteScores.size() > 1) {
                // Remove smallest
                autocompleteScores.remove(Collections.min(autocompleteScores));
                // Remove largest
                autocompleteScores.remove(Collections.max(autocompleteScores));
            }
            System.out.printf("Median autocomplete score: %d\n\n", autocompleteScores.get(0));
        } catch (Exception e) {
            System.out.printf("ERROR: %s\n", e.getMessage());
            e.printStackTrace();
        }
    }

    private final static Map<Character, Integer> autocompleteScoreLookup = Map.of(
            ')', 1,
            ']', 2,
            '}', 3,
            '>', 4
    );

    private static long calcAutocompleteScore(final List<Character> chars) {
        return chars.stream().reduce(0L, (acc, c) -> (acc * 5) + autocompleteScoreLookup.get(c), Long::sum);
    }
}
