package uk.daverichards.adventofcode2021.day24.alu;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

@EqualsAndHashCode
@ToString
public class State {
    private final Map<String, Long> register;
    private final long defaultValue;

    public State(final long defaultValue) {
        this.register = new HashMap<>();
        this.defaultValue = defaultValue;
    }

    public State(final Map<String, Long> register, final long defaultValue) {
        this.register = new HashMap<>(register);
        this.defaultValue = defaultValue;
    }

    public State set(final String key, final long val) {
        final Map<String, Long> newRegister = get();
        newRegister.put(key, val);
        return new State(newRegister, defaultValue);
    }

    public Map<String, Long> get() {
        return new HashMap<>(register);
    }

    public long get(final String key) {
        return register.getOrDefault(key, defaultValue);
    }
}
