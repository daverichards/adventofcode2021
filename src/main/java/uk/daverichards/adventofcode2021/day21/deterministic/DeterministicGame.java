package uk.daverichards.adventofcode2021.day21.deterministic;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
public class DeterministicGame {
    private static final int WINNING_SCORE = 1000;
    private static final int ROLLS_PER_TURN = 3;
    private static final int BOARD_SIZE = 10;

    private final Dice dice;
    private final List<Player> players = new ArrayList<>();
    private final Map<Integer, Integer> playerPositions = new HashMap<>();

    @Getter
    private boolean gameOver = false;

    public void addPlayer(final Player player, final int startingPosition) {
        int index = players.size();
        players.add(player);
        playerPositions.put(index, startingPosition);
    }

    public void playRound() {
        if (isGameOver()) {
            return;
        }
        for (int i = 0; i < players.size(); i++) {
            final int roll = roll();
            move(i, roll);
            if (isGameOver()) {
                return;
            }
        }
    }

    private int roll() {
        int score = 0;
        for (int i = 0; i < ROLLS_PER_TURN; i++) {
            score += dice.roll();
        }
        return score;
    }

    private void move(final int playerIndex, final int spaces) {
        int position = playerPositions.get(playerIndex);
        position += spaces;
        while (position > BOARD_SIZE) {
            position -= BOARD_SIZE;
        }
        playerPositions.put(playerIndex, position);
        players.get(playerIndex).incrementScore(position);

        if (players.get(playerIndex).getScore() >= WINNING_SCORE) {
            gameOver = true;
        }
    }
}
