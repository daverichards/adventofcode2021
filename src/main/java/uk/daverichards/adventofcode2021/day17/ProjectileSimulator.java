package uk.daverichards.adventofcode2021.day17;

import uk.daverichards.adventofcode2021.spatial.Box2d;
import uk.daverichards.adventofcode2021.spatial.Path2d;
import uk.daverichards.adventofcode2021.spatial.Point2d;
import uk.daverichards.adventofcode2021.spatial.Vector2d;

import java.util.List;

public class ProjectileSimulator {
    public static ProjectileResult simulate(final Point2d startingPosition, final Vector2d startingVelocity, final Box2d targetArea) {
        Path2d trajectory = new Path2d(List.of(startingPosition));
        Vector2d currentVelocity = startingVelocity;

        while (true) {
            Point2d currentPosition = trajectory.getLastPoint().add(currentVelocity);
            trajectory = trajectory.addPoint(currentPosition);
            currentVelocity = currentVelocity.add(new Vector2d(currentVelocity.x > 0 ? -1 : (currentVelocity.x < 0 ? 1 : 0), -1));

            if (targetArea.contains(currentPosition)) {
                // Hit
                return new ProjectileResult(startingVelocity, trajectory, true);
            }

            if (missed(currentPosition, currentVelocity, targetArea)) {
                // Miss
                return new ProjectileResult(startingVelocity, trajectory, false);
            }

            // Loop
        }
    }

    private static boolean missed(final Point2d currentPosition, final Vector2d currentVelocity, final Box2d targetArea) {
        if ((currentVelocity.x > 0 && currentPosition.x > targetArea.getMax().x)
            || (currentVelocity.x < 0 && currentPosition.x < targetArea.getMin().x)
            || (currentVelocity.x == 0 && (currentPosition.x < targetArea.getMin().x || currentPosition.x > targetArea.getMax().x))) {
            // Missed X
            return true;
        }
        if (currentVelocity.y < 0 && currentPosition.y < targetArea.getMin().y) {
            // Missed Y
            return true;
        }
        return false;
    }
}
