package uk.daverichards.adventofcode2021.day19;

import uk.daverichards.adventofcode2021.spatial.Point3d;
import uk.daverichards.adventofcode2021.util.FileReader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Day19 {
  public static void main(String[] args) {
      try {
          System.out.println("Day 19");
          List<String> input = FileReader.loadStringList("input/day19.txt");

          final List<Scanner> scanners = new ArrayList<>();
          Scanner scanner = null;
          for (String line : input) {
              if (line.isBlank()) {
                  continue;
              }
              if (line.startsWith("--- scanner ")) {
                  if (scanner != null) {
                      scanners.add(scanner);
                  }
                  scanner = new Scanner(Integer.parseInt(line.substring(12, 14).trim()));
                  continue;
              }
              List<String> parts = List.of(line.split(","));
              if (parts.size() != 3) {
                  throw new RuntimeException(String.format("Unexpected string '%s'", line));
              }
              if (scanner == null) {
                  throw new RuntimeException("No Scanner initialised");
              }
              scanner.addBeaconRelativePosition(new Point3d(
                  Integer.parseInt(parts.get(0)),
                  Integer.parseInt(parts.get(1)),
                  Integer.parseInt(parts.get(2))
              ));
          }
          if (scanner != null) {
              scanners.add(scanner);
          }

          scanners.get(0).setPosition(new Point3d(0, 0, 0));

          System.out.println("Part 1:");
          alignScanners(scanners);
          final Set<Point3d> beaconPositions = new HashSet<>();
          scanners.forEach(s -> beaconPositions.addAll(s.getBeaconAbsolutePositions()));

          System.out.format("Number of unique beacons: %d\n\n", beaconPositions.size());

          System.out.println("Part 2:");
          final Set<Integer> distances = new HashSet<>();
          scanners.forEach(a -> {
              scanners.forEach(b -> {
                distances.add(a.getPosition().to(b.getPosition()).manhattanMagnitude());
              });
          });

          System.out.format("Largest distance between scanners: %d\n\n", Collections.max(distances));
      } catch (Exception e) {
          System.out.printf("ERROR: %s\n", e.getMessage());
          e.printStackTrace();
      }
  }

  private static void alignScanners(final List<Scanner> scanners) {
      while (scanners.stream().anyMatch(s -> s.getPosition() == null)) {
          scanners.stream().filter(s -> s.getPosition() != null).forEach(positioned -> scanners.stream().filter(s -> s.getPosition() == null).forEach(positioned::align));
      }
  }
}
