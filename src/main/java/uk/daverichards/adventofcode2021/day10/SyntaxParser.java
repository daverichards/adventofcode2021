package uk.daverichards.adventofcode2021.day10;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SyntaxParser {
    private final Map<Character, Character> chunkPairs = new HashMap<>(Map.of(
            '(', ')',
            '[', ']',
            '{', '}',
            '<', '>'
    ));

    public List<Character> parseLine(List<Character> chars) {
        List<Character> openChunks = new ArrayList<>();
        for (Character c : chars) {
            if (chunkPairs.containsKey(c)) {
                // Open
                openChunks.add(c);
                continue;
            }
            if (chunkPairs.containsValue(c)) {
                // Close
                int currentOpenChunkIndex = openChunks.size() - 1;
                char expectedCharacter = chunkPairs.get(openChunks.get(currentOpenChunkIndex));
                if (expectedCharacter != c) {
                    // Unexpected closing character
                    throw new SyntaxCorruptException(String.format("Expected %c, but found %c instead", expectedCharacter, c), c, expectedCharacter);
                }
                openChunks.remove(currentOpenChunkIndex);
                continue;
            }
            // Invalid character
            throw new RuntimeException(String.format("Unexpected character: %c", c));
        }
        final List<Character> autocomplete = new ArrayList<>();
        if (!openChunks.isEmpty()) {
            for (int i = openChunks.size() - 1; i >= 0; i--) {
                autocomplete.add(chunkPairs.get(openChunks.get(i)));
            }
        }
        return autocomplete;
    }
}
