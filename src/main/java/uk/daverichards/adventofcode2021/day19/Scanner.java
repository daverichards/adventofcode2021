package uk.daverichards.adventofcode2021.day19;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import uk.daverichards.adventofcode2021.spatial.Point3d;
import uk.daverichards.adventofcode2021.spatial.Vector3d;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Getter
@ToString
public class Scanner {
    private static final int MIN_BEACON_MATCH = 12;

    private final int id;
    private Point3d position = null;
    private Function<Vector3d, Vector3d> orientationTransformation = null;
    private final Set<Point3d> beaconRelativePositions = new HashSet<>();
    private final Set<Point3d> beaconAbsolutePositions = new HashSet<>();

    private final Set<Function<Vector3d, Vector3d>> orientationTransformations = Set.of(
        v -> new Vector3d(-v.getX(), -v.getY(), v.getZ()),
        v -> new Vector3d(-v.getX(), -v.getZ(), -v.getY()),
        v -> new Vector3d(-v.getX(), v.getY(), -v.getZ()),
        v -> new Vector3d(-v.getX(), v.getZ(), v.getY()),
        v -> new Vector3d(-v.getY(), -v.getX(), -v.getZ()),
        v -> new Vector3d(-v.getY(), -v.getZ(), v.getX()),
        v -> new Vector3d(-v.getY(), v.getX(), v.getZ()),
        v -> new Vector3d(-v.getY(), v.getZ(), -v.getX()),
        v -> new Vector3d(-v.getZ(), -v.getX(), v.getY()),
        v -> new Vector3d(-v.getZ(), -v.getY(), -v.getX()),
        v -> new Vector3d(-v.getZ(), v.getX(), -v.getY()),
        v -> new Vector3d(-v.getZ(), v.getY(), v.getX()),
        v -> new Vector3d(v.getX(), -v.getY(), -v.getZ()),
        v -> new Vector3d(v.getX(), -v.getZ(), v.getY()),
        v -> new Vector3d(v.getX(), v.getY(), v.getZ()),
        v -> new Vector3d(v.getX(), v.getZ(), -v.getY()),
        v -> new Vector3d(v.getY(), -v.getX(), v.getZ()),
        v -> new Vector3d(v.getY(), -v.getZ(), -v.getX()),
        v -> new Vector3d(v.getY(), v.getX(), -v.getZ()),
        v -> new Vector3d(v.getY(), v.getZ(), v.getX()),
        v -> new Vector3d(v.getZ(), -v.getX(), -v.getY()),
        v -> new Vector3d(v.getZ(), -v.getY(), v.getX()),
        v -> new Vector3d(v.getZ(), v.getX(), v.getY()),
        v -> new Vector3d(v.getZ(), v.getY(), -v.getX())
    );

    public void addBeaconRelativePosition(Point3d v) {
        beaconRelativePositions.add(v);
    }

    public void setPosition(final Point3d position) {
        if (this.position != null && this.position != position) {
            throw new RuntimeException(String.format("Unable to set position to %s, already set to %s", position, this.position));
        }
        this.position = position;
        calculateBeaconAbsolutePositions();
    }

    public void setOrientationTransformation(final Function<Vector3d, Vector3d> orientationTransformation) {
        final Vector3d testVector = new Vector3d(1, 2, 3);
        if (this.orientationTransformation != null && !this.orientationTransformation.apply(testVector).equals(orientationTransformation.apply(testVector))) {
            throw new RuntimeException(String.format("Unable to set orientation to %s, already set to %s", orientationTransformation, this.orientationTransformation));
        }
        this.orientationTransformation = orientationTransformation;
        calculateBeaconAbsolutePositions();
    }

    private void calculateBeaconAbsolutePositions() {
        if (position == null || orientationTransformation == null) {
            return;
        }
        beaconRelativePositions.forEach(p -> beaconAbsolutePositions.add(position.add(orientationTransformation.apply(p.toVector()))));
    }

    public boolean align(Scanner other) {
        if (position == null) {
            throw new RuntimeException(String.format("Cannot align another scanner to scanner #%d, no position set", id));
        }
        if (id == other.getId()) {
            return false;
        }
        for (Function<Vector3d, Vector3d> thisOri : orientationTransformation != null ? Set.of(orientationTransformation) : orientationTransformations) {
            for (Function<Vector3d, Vector3d> otherOri : orientationTransformations) {
                final Set<Vector3d> thisVectors = beaconRelativePositions.stream().map(Point3d::toVector).map(thisOri).collect(Collectors.toSet());
                final Set<Vector3d> otherVectors = other.beaconRelativePositions.stream().map(Point3d::toVector).map(otherOri).collect(Collectors.toSet());
                for (Vector3d thisVector : thisVectors) {
                    final Set<Vector3d> possibleVectors = otherVectors.stream().map(thisVector::to).collect(Collectors.toSet());
                    for (Vector3d possibleVector : possibleVectors) {
                        final long matches = thisVectors.stream().map(v -> v.add(possibleVector)).filter(otherVectors::contains).count();
                        if (matches >= MIN_BEACON_MATCH) {
                            // Match
                            setOrientationTransformation(thisOri);
                            other.setPosition(position.add(possibleVector.opposite()));
                            other.setOrientationTransformation(otherOri);
//                            System.out.format("#%d had aligned #%d\n", id, other.id);
//                            System.out.format("#%d position: %s\n", id, position);
//                            System.out.format("#%d position: %s\n\n", other.id, other.position);
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
}
