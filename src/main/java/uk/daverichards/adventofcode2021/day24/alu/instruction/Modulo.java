package uk.daverichards.adventofcode2021.day24.alu.instruction;

import uk.daverichards.adventofcode2021.day24.alu.InputBuffer;
import uk.daverichards.adventofcode2021.day24.alu.State;

public class Modulo implements Instruction {
    private final String addr;
    private final long addLiteral;
    private final String addAddr;

    public Modulo(final String addr, final long literal) {
        this.addr = addr;
        addLiteral = literal;
        addAddr = null;
    }

    public Modulo(final String addr, final String otherAddr) {
        this.addr = addr;
        addLiteral = 0;
        addAddr = otherAddr;
    }

    @Override
    public State execute(State state, InputBuffer inputBuffer) {
        if (addAddr == null) {
            return state.set(addr, state.get(addr) % addLiteral);
        }
        return state.set(addr, state.get(addr) % state.get(addAddr));
    }
}
