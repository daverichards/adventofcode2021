package uk.daverichards.adventofcode2021.day23;

public interface Space {
    boolean isHallway();
    boolean isRoom();
    boolean canBeOccupiedBy(final Amphipod amphipod);
}
