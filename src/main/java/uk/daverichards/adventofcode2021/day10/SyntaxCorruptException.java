package uk.daverichards.adventofcode2021.day10;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SyntaxCorruptException extends RuntimeException {
    private final char illegalCharacter;
    private final char expectedCharacter;

    public SyntaxCorruptException(String message, char illegalCharacter, char expectedCharacter) {
        super(message);
        this.illegalCharacter = illegalCharacter;
        this.expectedCharacter = expectedCharacter;
    }
}
