package uk.daverichards.adventofcode2021.day16;

import uk.daverichards.adventofcode2021.day16.packet.Header;
import uk.daverichards.adventofcode2021.day16.packet.Literal;
import uk.daverichards.adventofcode2021.day16.packet.Operator;
import uk.daverichards.adventofcode2021.day16.packet.Packet;
import uk.daverichards.adventofcode2021.util.Utils;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PacketDecoder {
    private final Map<String, String> hexToBin = new HashMap<>();

    public PacketDecoder() {
        // Populate hexToBin
        for (int i = 0; i < 16; i++) {
            hexToBin.put(Integer.toHexString(i).toUpperCase(), Utils.padLeft(Integer.toBinaryString(i), 4, '0'));
        }
    }

    public Packet parsePacketFromHex(final String hex) {
        final StringBuilder binBuilder = new StringBuilder();
        for (int i = 0; i < hex.length(); i++) {
            binBuilder.append(hexToBin.get(hex.substring(i, i+1)));
        }
        return parsePacketFromBin(new PacketStringReader(binBuilder.toString()));
    }

    private Packet parsePacketFromBin(final PacketStringReader bin) {
        if (bin.length() < 11) {
            throw new InvalidParameterException(String.format("Packet should be at least 11 digits long, got %d (%s)", bin.length(), bin));
        }

        final Header header = new Header(
            Integer.parseInt(bin.pop(3), 2),
            Integer.parseInt(bin.pop(3), 2)
        );

        if (header.isLiteral()) {
            // Literal
            final StringBuilder literalBuilder = new StringBuilder();
            boolean endOfLiteral = false;
            while (!endOfLiteral) {
                endOfLiteral = "0".equals(bin.pop(1));
                literalBuilder.append(bin.pop(4));
            }
            return new Literal(header, Long.parseLong(literalBuilder.toString(), 2));
        }

        // Operator
        final String lengthTypeId = bin.pop(1);
        final List<Packet> subPackets = new ArrayList<>();

        if ("0".equals(lengthTypeId)) {
            // Fixed length
            final int bitLength = Integer.parseInt(bin.pop(15), 2);
            final PacketStringReader subPacketBits = new PacketStringReader(bin.pop(bitLength));
            while (!subPacketBits.isEmpty()) {
                subPackets.add(
                    parsePacketFromBin(subPacketBits)
                );
            }
        } else {
            // Sub-Packet length
            final int subPacketLength = Integer.parseInt(bin.pop(11), 2);
            for (int i = 0; i < subPacketLength; i++) {
                subPackets.add(
                    parsePacketFromBin(bin)
                );
            }
        }

        return new Operator(header, subPackets);
    }
}
