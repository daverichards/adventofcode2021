package uk.daverichards.adventofcode2021.day24.modelnumber;

import lombok.Data;

@Data
public class ProgramVariable {
    private final long chk;
    private final long div;
    private final long add;
}
