package uk.daverichards.adventofcode2021.day04.bingo;

import java.util.HashMap;
import java.util.Map;

public class NumberRepository {
    private final Map<Integer, Number> numbers = new HashMap<>();

    public NumberRepository(int min, int max) {
        for (int i = min; i <= max; i++) {
            numbers.put(i, new Number(i));
        }
    }

    public Number get(int i) {
        return numbers.get(i);
    }
}
