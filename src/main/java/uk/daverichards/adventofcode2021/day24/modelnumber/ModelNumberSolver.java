package uk.daverichards.adventofcode2021.day24.modelnumber;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

public class ModelNumberSolver {
    private static final int MODEL_NUMBER_LENGTH = 14;

    private final List<ProgramVariable> programVariables = List.of(
        new ProgramVariable(11, 1, 7),
        new ProgramVariable(14, 1, 8),
        new ProgramVariable(10, 1, 16),
        new ProgramVariable(14, 1, 8),
        new ProgramVariable(-8, 26, 3),
        new ProgramVariable(14, 1, 12),
        new ProgramVariable(-11, 26, 1),
        new ProgramVariable(10, 1, 8),
        new ProgramVariable(-6, 26, 8),
        new ProgramVariable(-9, 26, 14),
        new ProgramVariable(12, 1, 4),
        new ProgramVariable(-5, 26, 14),
        new ProgramVariable(-4, 26, 15),
        new ProgramVariable(-9, 26, 6)
    );

    private final List<Integer> upperZLimits = List.of(
        7,
        7,
        7,
        7,
        7,
        6,
        6,
        5,
        5,
        4,
        3,
        3,
        2,
        1
    );

    public long findLargestModelNumber() throws InvalidModelNumberException {
        return findModelNumber(Math::max);
    }

    public long findSmallestModelNumber() throws InvalidModelNumberException {
        return findModelNumber((a, b) -> Math.min(a, b) < 0 ? Math.max(a, b) : Math.min(a, b));
    }

    private long findModelNumber(final BiFunction<Long, Long, Long> comparision) throws InvalidModelNumberException {
        final Map<Long, Long> results = new HashMap<>(Map.of(0L, 0L));
        for (int i = 0; i < MODEL_NUMBER_LENGTH; i++) {
            final Map<Long, Long> digitResults = new HashMap<>();
            final ProgramVariable vars = programVariables.get(i);
            for (long inp = 1; inp < 10; inp++) {
                for (Map.Entry<Long, Long> e : results.entrySet()) {
                    final long z = inputFunc(vars.getChk(), vars.getAdd(), vars.getDiv(), inp, e.getKey());
                    digitResults.put(z, comparision.apply(digitResults.getOrDefault(z, -1L), (e.getValue() * 10) + inp));
                }
            }
            results.clear();
            results.putAll(digitResults);
        }
        if (results.containsKey(0L)) {
            return results.get(0L);
        }
        throw new InvalidModelNumberException("No valid model numbers found!");
    }

    private long inputFunc(long chk, long add, long div, long inp, long z) {
        if (((z % 26L) + chk) == inp) {
            // Input is equal to result of previous operation remainder from /26 and + chk
            // (z % 26) is equivalent to getting the last 'bit' in base 26
            // If this last 'bit' == inp - chk, then perform the below
            // When div == 1; z remains the same.
            // When div == 26; bitshift >> in base 26
            return z / div;
        } else {
            // Means the next operation (z % 26) will equal (inp + add)
            //  Therefore the next operation will check (this.inp + this.add + next.chk == next.inp)
            // (z / 1) * 26 is equivalent to a bitshift << in base 26, (inp + add) becoming the least significant bit
            // (z / 26) * 26 is equivalent to replacing the least significant bit with (inp + add)
            return ((z / div) * 26L) + (inp + add);
        }
    }
}
