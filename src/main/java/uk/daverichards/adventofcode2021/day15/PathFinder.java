package uk.daverichards.adventofcode2021.day15;

import lombok.RequiredArgsConstructor;
import uk.daverichards.adventofcode2021.spatial.Map2d;
import uk.daverichards.adventofcode2021.spatial.Point2d;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class PathFinder {
    private final Map2d<Integer> riskMap;
    private final Set<Path> paths = new HashSet<>();
    private final Set<Point2d> visitedPoints = new HashSet<>();

    public Path findLowestRiskPath(final Point2d start, final Point2d end) {
        // Reset paths
        paths.clear();
        visitedPoints.clear();
        // Add start (with 0 value)
        paths.add(new Path(List.of(start), 0));

        int steps = 0;
        while (true) {
            steps++;
            // Find the path in list with the lowest score
            final Path lowestPath = popLowestValuePath(end);

            // Find neighbours of last point that aren't already in the path
            // If this point is already in another path, then skip it (the other path would have got to this point quicker)
            final Map<Point2d, Integer> neighbours = riskMap.getNeighbours(lowestPath.getLastPoint()).entrySet()
                .stream().filter(e -> !visitedPoints.contains(e.getKey()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

            if (neighbours.isEmpty()) {
                continue;
            }

            // if end has been found, return path
            if (neighbours.containsKey(end)) {
                System.out.printf("Pathfinder steps: %d\n", steps); //34567 v 43630 v 9996
                return lowestPath.addPoint(end, neighbours.get(end));
            }

            // For each neighbour, create new path and add to list
            neighbours.forEach((p, v) -> {
                paths.add(
                    lowestPath.addPoint(p, v)
                );
                visitedPoints.add(p);
            });

            // Repeat
        }
    }

    private Path popLowestValuePath(Point2d end) {
        Path lowestPath = null;
        int lowestPathScore = 0;
        for (Path p : paths) {
            // Get current score, and presume all 1s to the end
            //final int predictedLowestValue = p.getPathScore() + p.getLastPoint().vectorTo(end).getManhattanDistance();
            // Get current score
            final int predictedLowestValue = p.getPathScore();
            if (lowestPath == null || predictedLowestValue < lowestPathScore) {
                lowestPath = p;
                lowestPathScore = predictedLowestValue;
            }
        }
        // Remove path
        paths.remove(lowestPath);
        return lowestPath;
    }

    private boolean pointInExistingPath(Point2d point) {
        for (Path path : paths) {
            if (path.contains(point)) {
                return true;
            }
        }
        return false;
    }
}
