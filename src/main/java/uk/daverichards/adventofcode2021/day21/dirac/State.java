package uk.daverichards.adventofcode2021.day21.dirac;

import lombok.Value;

@Value
public class State {
    int p1Position;
    int p1Score;
    int p2Position;
    int p2Score;

    public State setPosition(final int i, final int position) {
        if (i == 1) {
            return new State(position, p1Score, p2Position, p2Score);
        }
        if (i == 2) {
            return new State(p1Position, p1Score, position, p2Score);
        }
        throw new IllegalArgumentException(String.format("setPosition: Unknown player '%d'", i));
    }

    public int getPosition(final int i) {
        if (i == 1) {
            return getP1Position();
        }
        if (i == 2) {
            return getP2Position();
        }
        throw new IllegalArgumentException(String.format("getPosition: Unknown player '%d'", i));
    }

    public State incrementScore(final int i, final int score) {
        if (i == 1) {
            return new State(p1Position, p1Score + score, p2Position, p2Score);
        }
        if (i == 2) {
            return new State(p1Position, p1Score, p2Position, p2Score + score);
        }
        throw new IllegalArgumentException(String.format("incrementScore: Unknown player '%d'", i));
    }

    public int getScore(final int i) {
        if (i == 1) {
            return getP1Score();
        }
        if (i == 2) {
            return getP2Score();
        }
        throw new IllegalArgumentException(String.format("getScore: Unknown player '%d'", i));
    }
}
