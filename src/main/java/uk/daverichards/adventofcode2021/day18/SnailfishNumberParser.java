package uk.daverichards.adventofcode2021.day18;

import uk.daverichards.adventofcode2021.util.StringFeeder;

public class SnailfishNumberParser {
    public static SnailfishNumber parse(final String s) {
        return parse(StringFeeder.create(s));
    }

    public static SnailfishNumber parse(final StringFeeder s) {
        // Expecting [ or literal
        final String open = s.popLeft(1);
        if (open.matches("^[0-9]$")) {
            // A literal
            return new SnailfishNumberLiteral(Integer.parseInt(open));
        }
        if (!"[".equals(open)) {
            throw new RuntimeException(String.format("Was expecting either a literal or '[', got '%s'", open));
        }
        // A pair
        // Left
        final SnailfishNumber left = parse(s);

        // Separator
        final String separator = s.popLeft(1);
        if (!",".equals(separator)) {
            throw new RuntimeException(String.format("Was expecting ',', got '%s'", separator));
        }

        // Right
        final SnailfishNumber right = parse(s);

        // Closing pair
        final String close = s.popLeft(1);
        if (!"]".equals(close)) {
            throw new RuntimeException(String.format("Was expecting ']', got '%s'", close));
        }

        return new SnailfishNumberPair(left, right);
    }
}
