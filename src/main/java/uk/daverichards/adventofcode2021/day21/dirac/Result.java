package uk.daverichards.adventofcode2021.day21.dirac;

import lombok.Value;

@Value
public class Result {
    long playerOneWins;
    long playerTwoWins;

    public Result incrementPlayerWins(final int i) {
        if (i == 1) {
            return new Result(playerOneWins + 1, playerTwoWins);
        }
        if (i == 2) {
            return new Result(playerOneWins, playerTwoWins + 1);
        }
        throw new IllegalArgumentException(String.format("Unknown player '%d' result", i));
    }

    public Result merge(final Result other) {
        return new Result(playerOneWins + other.playerOneWins, playerTwoWins + other.playerTwoWins);
    }
}
