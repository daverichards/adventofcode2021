package uk.daverichards.adventofcode2021.spatial;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class Box2d {
    private Point2d min;
    private Point2d max;

    public Box2d(int minX, int minY, int maxX, int maxY) {
        min = new Point2d(Math.min(minX, maxX), Math.min(minY, maxY));
        max = new Point2d(Math.max(minX, maxX), Math.max(minY, maxY));
    }

    public boolean contains(Point2d p) {
        return p.x >= min.x && p.x <= max.x && p.y >= min.y && p.y <= max.y;
    }

    public List<Point2d> getPoints() {
        final List<Point2d> points = new ArrayList<>();
        for (int x = min.x; x <= max.x; x++) {
            for (int y = min.y; y <= max.y; y++) {
                points.add(new Point2d(x, y));
            }
        }
        return points;
    }
}
