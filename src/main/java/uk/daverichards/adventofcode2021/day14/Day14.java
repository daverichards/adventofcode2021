package uk.daverichards.adventofcode2021.day14;

import uk.daverichards.adventofcode2021.util.FileReader;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Day14 {
    public static void main(String[] args) {
        try {
            System.out.println("Day 14");
            List<String> input = FileReader.loadStringList("input/day14.txt");

            final String template = input.get(0);
            final PolymerGenerator polymerGenerator = new PolymerGenerator();
            for (int i = 2; i < input.size(); i++) {
                final String[] parts = input.get(i).split(" -> ");
                polymerGenerator.addRule(parts[0], parts[1]);
            }

            System.out.println("Part 1:");
            final Map<Character, Long> p1CharFrequencies = polymerGenerator.generateCharFrequenciesFromTemplate(template, 10);
            System.out.println(p1CharFrequencies);
            final long p1LargestFrequency = Collections.max(p1CharFrequencies.values());
            final long p1SmallestFrequency = Collections.min(p1CharFrequencies.values());
            System.out.printf("%d - %d = %d\n\n", p1LargestFrequency, p1SmallestFrequency, p1LargestFrequency - p1SmallestFrequency);

            System.out.println("Part 2:");
            final Map<Character, Long> p2CharFrequencies = polymerGenerator.generateCharFrequenciesFromTemplate(template, 40);
            System.out.println(p2CharFrequencies);
            final long p2LargestFrequency = Collections.max(p2CharFrequencies.values());
            final long p2SmallestFrequency = Collections.min(p2CharFrequencies.values());
            System.out.printf("%d - %d = %d\n\n", p2LargestFrequency, p2SmallestFrequency, p2LargestFrequency - p2SmallestFrequency);

        } catch (Exception e) {
            System.out.printf("ERROR: %s\n", e.getMessage());
            e.printStackTrace();
        }
    }
}
