package uk.daverichards.adventofcode2021.day24.alu;

import uk.daverichards.adventofcode2021.day24.alu.instruction.Instruction;

public class ALU {
    public static State run(final Program program, final InputBuffer inputBuffer) {
        State state = new State(0);
        for (final Instruction i : program.getInstructions()) {
            state = i.execute(state, inputBuffer);
        }
        return state;
    }
}
