package uk.daverichards.adventofcode2021.day17;

import lombok.Data;
import uk.daverichards.adventofcode2021.spatial.Path2d;
import uk.daverichards.adventofcode2021.spatial.Vector2d;

@Data
public class ProjectileResult {
    private final Vector2d initialVelocity;
    private final Path2d trajectory;
    private final boolean hit;
}
