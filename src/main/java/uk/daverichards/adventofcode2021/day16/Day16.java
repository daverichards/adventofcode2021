package uk.daverichards.adventofcode2021.day16;

import uk.daverichards.adventofcode2021.day16.packet.Packet;
import uk.daverichards.adventofcode2021.util.FileReader;

public class Day16 {
    public static void main(String[] args) {
        try {
            System.out.println("Day 16");
            String input = FileReader.loadStringList("input/day16.txt").get(0);

            final PacketDecoder packetDecoder = new PacketDecoder();

            Packet packet = packetDecoder.parsePacketFromHex(input);
            System.out.println(packet);

            System.out.println("Part 1:");
            System.out.printf("Version sum of packet: %d\n\n", packet.getVersionSum());

            System.out.println("Part 2:");
            System.out.printf("Evaluated value: %d\n\n", packet.getValue());
        } catch (Exception e) {
            System.out.printf("ERROR: %s\n", e.getMessage());
            e.printStackTrace();
        }
    }
}
