package uk.daverichards.adventofcode2021.day18;

import uk.daverichards.adventofcode2021.util.FileReader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Day18 {
    public static void main(String[] args) {
        try {
            System.out.println("Day 18");
            List<String> input = FileReader.loadStringList("input/day18.txt");

            final List<SnailfishNumber> snailfishNumbers = input.stream().map(SnailfishNumberParser::parse).collect(Collectors.toList());

            System.out.println("Part 1:");
            SnailfishNumber sum = snailfishNumbers.get(0);
            for (int i = 1; i < snailfishNumbers.size(); i++) {
                sum = SnailfishMath.add(sum, snailfishNumbers.get(i));
            }
            System.out.format("Sum: %s\n", sum);
            System.out.format("Sum Magnitude: %s\n\n", sum.getMagnitude());

            System.out.println("Part 2:");
            final List<Long> magnitudes = new ArrayList<>();
            for (int i = 0; i < snailfishNumbers.size(); i++) {
                for (int j = 0; j < snailfishNumbers.size(); j++) {
                    if (i == j) {
                        continue;
                    }
                    magnitudes.add(SnailfishMath.add(snailfishNumbers.get(i), snailfishNumbers.get(j)).getMagnitude());
                }
            }
            System.out.println(magnitudes.size());
            System.out.format("Largest magnitude from any sum: %d\n\n", Collections.max(magnitudes));
        } catch (Exception e) {
            System.out.printf("ERROR: %s\n", e.getMessage());
            e.printStackTrace();
        }
    }
}
