package uk.daverichards.adventofcode2021.day07;

import uk.daverichards.adventofcode2021.util.FileReader;

import java.util.*;
import java.util.stream.Collectors;

public class Day07 {
    public static void main(String[] args) {
        try {
            System.out.println("Day 07");
            List<String> input = FileReader.loadStringList("input/day07.txt");
            List<Integer> crabPositions = Arrays.stream(input.get(0).split(",")).map(Integer::parseInt).collect(Collectors.toList());

            int minPosition = crabPositions.stream().mapToInt(i -> i).min().orElseThrow(NoSuchElementException::new);
            int maxPosition = crabPositions.stream().mapToInt(i -> i).max().orElseThrow(NoSuchElementException::new);

            System.out.println("Part 1:");
            int p1MinFuelSpent = 0;
            int p1MinFuelPosition = 0;
            for (int i = minPosition; i <= maxPosition; i++) {
                int fuelSpent = getFuelSpent(crabPositions, i);
                if (p1MinFuelSpent == 0 || fuelSpent < p1MinFuelSpent) {
                    p1MinFuelSpent = fuelSpent;
                    p1MinFuelPosition = i;
                }
            }
            System.out.printf("Position %d requires %d fuel spent\n\n", p1MinFuelPosition, p1MinFuelSpent);

            System.out.println("Part 2:");
            Map<Integer, Integer> fuelLookup = new HashMap<>(Map.of(0, 0));
            for (int i = 1; i <= (maxPosition - minPosition); i++) {
                fuelLookup.put(i, fuelLookup.get(i - 1) + i);
            }
            int p2MinFuelSpent = 0;
            int p2MinFuelPosition = 0;
            for (int i = minPosition; i <= maxPosition; i++) {
                int fuelSpent = getFuelSpentWithLookup(crabPositions, i, fuelLookup);
                if (p2MinFuelSpent == 0 || fuelSpent < p2MinFuelSpent) {
                    p2MinFuelSpent = fuelSpent;
                    p2MinFuelPosition = i;
                }
            }
            System.out.printf("Position %d requires %d fuel spent\n\n", p2MinFuelPosition, p2MinFuelSpent);

        } catch (Exception e) {
            System.out.printf("ERROR: %s\n", e.getMessage());
            e.printStackTrace();
        }
    }

    private static int getFuelSpent(final List<Integer> crabPositions, final int destination) {
        return crabPositions.stream().reduce(0, (acc, crabPosition) -> acc + Math.abs(crabPosition - destination));
    }

    private static int getFuelSpentWithLookup(final List<Integer> crabPositions, final int destination, Map<Integer, Integer> fuelLookup) {
        return crabPositions.stream().reduce(0, (acc, crabPosition) -> acc + fuelLookup.get(Math.abs(crabPosition - destination)));
    }
}
