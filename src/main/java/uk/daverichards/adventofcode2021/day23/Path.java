package uk.daverichards.adventofcode2021.day23;

import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode
public class Path {
    private final List<State> steps;
    private final List<Integer> energySpent;

    public Path(final State firstStep, final int firstEnergySpent) {
        this.steps = List.of(firstStep);
        this.energySpent = List.of(firstEnergySpent);
    }

    public Path(final List<State> steps, final List<Integer> energySpent) {
        this.steps = new ArrayList<>(steps);
        this.energySpent = new ArrayList<>(energySpent);
    }

    public List<State> getSteps() {
        return new ArrayList<>(steps);
    }

    public State getLastStep() {
        if (steps.isEmpty()) {
            return null;
        }
        return steps.get(steps.size() - 1);
    }

    public boolean contains(final State step) {
        return steps.contains(step);
    }

    public List<Integer> getEnergySpent() {
        return new ArrayList<>(energySpent);
    }


    public int getTotalEnergySpent() {
        if (energySpent.isEmpty()) {
            return 0;
        }
        return energySpent.get(energySpent.size() - 1);
    }

    public int getEnergySpentAtStep(final State step) {
        if (!steps.contains(step)) {
            return 0;
        }
        return energySpent.get(steps.indexOf(step));
    }

    public Path addStep(final State state, final int energySpent) {
        final List<State> newSteps = getSteps();
        newSteps.add(state);
        final List<Integer> newEnegrySpent = getEnergySpent();
        newEnegrySpent.add(newEnegrySpent.get(newEnegrySpent.size() - 1) + energySpent);
        return new Path(newSteps, newEnegrySpent);
    }
}
