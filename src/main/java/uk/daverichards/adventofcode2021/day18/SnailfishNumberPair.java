package uk.daverichards.adventofcode2021.day18;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SnailfishNumberPair implements SnailfishNumber {
    SnailfishNumber left;
    SnailfishNumber right;

    @Override
    public SnailfishNumber copy() {
        return new SnailfishNumberPair(left.copy(), right.copy());
    }

    @Override
    public boolean isLiteral() {
        return false;
    }

    @Override
    public SnailfishNumberLiteral getLiteral() {
        throw new UnsupportedOperationException("Cannot treat Snailfish Number Pair as a Literal");
    }

    @Override
    public boolean isPair() {
        return true;
    }

    @Override
    public SnailfishNumberPair getPair() {
        return this;
    }

    @Override
    public boolean isExplodable() {
        return left.isLiteral() && right.isLiteral();
    }

    @Override
    public long getMagnitude() {
        return (left.getMagnitude() * 3) + (right.getMagnitude() * 2);
    }

    @Override
    public String toString() {
        return "[" + left + "," + right + "]";
    }
}
