package uk.daverichards.adventofcode2021.day21.dirac;

import lombok.Getter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DiracGame {
    private static final int WINNING_SCORE = 21;
    private static final int ROLLS_PER_TURN = 3;
    private static final int BOARD_SIZE = 10;

    private final List<Integer> possibleScores = List.of(3,4,5,4,5,6,5,6,7,4,5,6,5,6,7,6,7,8,5,6,7,6,7,8,7,8,9);

    private final Map<Boolean, Map<State, Result>> cache = new HashMap<>();

    public Result play(final int playerOnePosition, final int playerTwoPosition) {
        return play(new State(playerOnePosition, 0, playerTwoPosition, 0), true);
    }

    private Result play(final State state, final boolean playerOneTurn) {
        if (isCached(state, playerOneTurn)) {
            return getCache(state, playerOneTurn);
        }

        final int playerIndex = playerOneTurn ? 1 : 2;

        Result result = new Result(0, 0);

        for (int score : possibleScores) {
            final int newPosition = getNewPosition(state.getPosition(playerIndex), score);
            if (state.getScore(playerIndex) + newPosition >= WINNING_SCORE) {
                result = result.incrementPlayerWins(playerIndex);
                continue;
            }

            result = result.merge(
                play(
                    state.setPosition(playerIndex, newPosition).incrementScore(playerIndex, newPosition),
                    !playerOneTurn
                )
            );
        }

        setCache(state, playerOneTurn, result);

        return result;
    }

    private int getNewPosition(final int currentPosition, final int moves) {
        int newPosition = currentPosition + moves;
        if (newPosition > BOARD_SIZE) {
            newPosition -= BOARD_SIZE;
        }
        return newPosition;
    }

    private boolean isCached(final State state, final boolean playerOneTurn) {
        return cache.containsKey(playerOneTurn) && cache.get(playerOneTurn).containsKey(state);
    }

    private Result getCache(final State state, final boolean playerOneTurn) {
        if (!cache.containsKey(playerOneTurn)) {
            return null;
        }
        return cache.get(playerOneTurn).get(state);
    }

    private void setCache(final State state, final boolean playerOneTurn, final Result result) {
        if (!cache.containsKey(playerOneTurn)) {
            cache.put(playerOneTurn, new HashMap<>());
        }
        cache.get(playerOneTurn).put(state, result);
    }
}
