package uk.daverichards.adventofcode2021.day01;

import uk.daverichards.adventofcode2021.util.FileReader;

import java.io.IOException;
import java.util.List;

public class Day01 {
  public static void main(String[] args) {
    try {
      System.out.println("Day 01");
      List<Integer> input = FileReader.loadIntegerList("input/day01.txt");

      System.out.println("Part 1:");
      Integer prev = null;
      Integer incrementCount = 0;
      for (Integer i: input) {
        if (prev != null && i > prev) {
          incrementCount++;
        }
        prev = i;
      }
      System.out.printf("Number of increments: %d\n", incrementCount);

      System.out.println("Part 2:");
      Integer incrementCount2 = 0;
      for (int i = 1; i <= input.size() - 3; i++) {
        int a = getWindowSum(input, i - 1);
        int b = getWindowSum(input, i);
        if (b > a) {
          incrementCount2++;
        }
      }
      System.out.printf("Number of window increments: %d\n", incrementCount2);

    } catch (Exception e) {
      System.out.printf("ERROR: %s\n", e.getMessage());
    }
  }

  private static Integer getWindowSum(List<Integer> data, int index) {
    if (index < 0 || index > (data.size() - 3)) {
      return null;
    }
    return data.get(index) + data.get(index + 1) + data.get(index + 2);
  }
}
