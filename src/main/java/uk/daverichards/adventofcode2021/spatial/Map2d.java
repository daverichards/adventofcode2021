package uk.daverichards.adventofcode2021.spatial;

import lombok.Getter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;

public class Map2d<T> {
    private final Map<Point2d, T> points = new HashMap<>();
    @Getter
    Point2d minBound = null;
    @Getter
    private Point2d maxBound = null;

    private final List<Vector2d> neighbourVectors = List.of(
            new Vector2d(-1, 0),
            new Vector2d(1, 0),
            new Vector2d(0, -1),
            new Vector2d(0, 1)
    );
    private final List<Vector2d> neighbourDiagonalVectors = List.of(
            new Vector2d(-1, -1),
            new Vector2d(-1, 1),
            new Vector2d(1, -1),
            new Vector2d(1, 1)
    );

    public Map2d<T> copy() {
        Map2d<T> copy = new Map2d<>();
        copy.points.putAll(points);
        copy.minBound = minBound;
        copy.maxBound = maxBound;
        return copy;
    }

    public void setPoint(final Point2d p, T v) {
        points.put(p, v);
        expandBounds(p);
    }

    public void setPoints(final Set<Point2d> ps, T v) {
        ps.forEach(p -> setPoint(p, v));
    }

    public void setPoints(final Box2d b, T v) {
        b.getPoints().forEach(p -> setPoint(p, v));
    }

    public T removePoint(final Point2d p) {
        return points.remove(p);
    }

    public void movePoint(final Point2d from, final Point2d to) {
        if (!pointExists(from)) {
            // No point to move
            return;
        }
        // Will override anything in the destination
        setPoint(to, removePoint(from));
    }

    public T getPoint(Point2d p) {
        return points.get(p);
    }

    public T getPointOrDefault(Point2d p, T dflt) {
        return points.getOrDefault(p, dflt);
    }

    public Map<Point2d, T> getPoints() {
        return new HashMap<>(points);
    }

    public boolean pointExists(Point2d p) {
        return points.containsKey(p);
    }

    private void expandBounds(Point2d p) {
        minBound = minBound == null ? p : new Point2d(Math.min(minBound.x, p.x), Math.min(minBound.y, p.y));
        maxBound = maxBound == null ? p : new Point2d(Math.max(maxBound.x, p.x), Math.max(maxBound.y, p.y));
    }

    public Map<Point2d, T> getNeighbours(final Point2d p) {
        // Does not include diagonals
        final Map<Point2d, T> neighbours = new HashMap<>();
        neighbourVectors.forEach(v -> {
            Point2d n = p.add(v);
            if (points.containsKey(n)) {
                neighbours.put(n, points.get(n));
            }
        });
        return neighbours;
    }

    public Map<Point2d, T> getNeighboursIncludingDiagonals(final Point2d p) {
        final Map<Point2d, T> neighbours = getNeighbours(p);
        // Add diagonals
        neighbourDiagonalVectors.forEach(v -> {
            Point2d n = p.add(v);
            if (points.containsKey(n)) {
                neighbours.put(n, points.get(n));
            }
        });
        return neighbours;
    }

    public Map2d<T> foldX(final int x, final BiFunction<T, T, T> combiner) {
        final Map2d<T> folded = new Map2d<>();
        points.forEach((p, v) -> {
            final int transposedX = p.x < x ? p.x : x - (p.x - x);
            final Point2d transposedPoint = new Point2d(transposedX, p.y);
            folded.setPoint(transposedPoint, combiner.apply(v, getPoint(transposedPoint)));
        });
        return folded;
    }

    public Map2d<T> foldY(final int y, final BiFunction<T, T, T> combiner) {
        final Map2d<T> folded = new Map2d<>();
        points.forEach((p, v) -> {
            final int transposedY = p.y < y ? p.y : y - (p.y - y);
            final Point2d transposedPoint = new Point2d(p.x, transposedY);
            folded.setPoint(transposedPoint, combiner.apply(v, getPoint(transposedPoint)));
        });
        return folded;
    }

    public String toString(final Function<T, String> mapper, final String dflt) {
        return toString(mapper, dflt, false);
    }

    public String toString(final Function<T, String> mapper, final String dflt, final boolean yDirectionUp) {
        // Currently x-axis -> right and y-axis -> down. If yDirectionUp == true, y-axis -> up
        final StringBuilder sb = new StringBuilder();
        for (int y = (yDirectionUp ? maxBound.y : minBound.y); yDirectionUp ? (y >= minBound.y) : ( y <= maxBound.y); y += (yDirectionUp ? -1 : 1)) {
            for (int x = minBound.x; x <= maxBound.x; x++) {
                Point2d p = new Point2d(x, y);
                if (points.containsKey(p)) {
                    sb.append(mapper.apply(points.get(p)));
                } else {
                    sb.append(dflt);
                }
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}
