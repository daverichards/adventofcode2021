package uk.daverichards.adventofcode2021.util;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class StringFeeder {
    private String s;

    public String popLeft(final int length) {
        final String popped = s.substring(0, length);
        s = s.substring(length);
        return popped;
    }

    public static StringFeeder create(final String s) {
        return new StringFeeder(s);
    }
}
