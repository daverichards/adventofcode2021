package uk.daverichards.adventofcode2021.day18;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SnailfishNumberLiteral implements SnailfishNumber {
    long value;

    @Override
    public SnailfishNumber copy() {
        return new SnailfishNumberLiteral(value);
    }

    @Override
    public boolean isLiteral() {
        return true;
    }

    @Override
    public SnailfishNumberLiteral getLiteral() {
        return this;
    }

    @Override
    public boolean isPair() {
        return false;
    }

    @Override
    public SnailfishNumberPair getPair() {
        throw new UnsupportedOperationException("Cannot treat Snailfish Number Literal as a Pair");
    }

    @Override
    public boolean isExplodable() {
        return false;
    }

    @Override
    public long getMagnitude() {
        return value;
    }

    @Override
    public String toString() {
        return Long.toString(value);
    }
}
