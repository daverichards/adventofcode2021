package uk.daverichards.adventofcode2021.day13;

import uk.daverichards.adventofcode2021.spatial.Map2d;
import uk.daverichards.adventofcode2021.spatial.Point2d;
import uk.daverichards.adventofcode2021.util.FileReader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

public class Day13 {
    public static void main(String[] args) {
        try {
            System.out.println("Day 13");
            List<String> input = FileReader.loadStringList("input/day13.txt");
            final int inputSplitIndex = input.indexOf("");

            final Map2d<Boolean> startingMap = new Map2d<>();
            for (int i = 0; i < inputSplitIndex; i++) {
                String[] parts = input.get(i).split(",");
                startingMap.setPoint(new Point2d(Integer.parseInt(parts[0]), Integer.parseInt(parts[1])), true);
            }

            final List<String> instructions = new ArrayList<>();
            for (int i = inputSplitIndex + 1; i < input.size(); i++) {
                instructions.add(input.get(i));
            }

            System.out.println("Part 1:");
            final Map2d<Boolean> afterOneFold = processInstruction(startingMap, instructions.get(0));
            System.out.println(afterOneFold.toString(b -> b ? "#" : ".", "."));
            System.out.printf("Number of points after one fold: %d\n\n", afterOneFold.getPoints().size());

            System.out.println("Part 2:");
            Map2d<Boolean> folded = startingMap.copy();
            for (int i = 0; i < instructions.size(); i++) {
                folded = processInstruction(folded, instructions.get(i));
            }
            System.out.println(folded.toString(b -> b ? "#" : ".", " "));

        } catch (Exception e) {
            System.out.printf("ERROR: %s\n", e.getMessage());
            e.printStackTrace();
        }
    }

    private static Map2d<Boolean> processInstruction(final Map2d<Boolean> m, final String instruction) {
        final BiFunction<Boolean, Boolean, Boolean> combiner = (a, b) -> a || b;
        if (instruction.startsWith("fold along x=")) {
            return m.foldX(Integer.parseInt(instruction.substring(13)), combiner);
        }
        if (instruction.startsWith("fold along y=")) {
            return m.foldY(Integer.parseInt(instruction.substring(13)), combiner);
        }
        throw new RuntimeException(String.format("Unknown instruction: %s", instruction));
    }
}
