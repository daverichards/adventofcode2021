package uk.daverichards.adventofcode2021.day04.bingo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class NumberSet {
    private final List<Number> numbers = new ArrayList<>();

    public void addNumber(Number number) {
        numbers.add(number);
    }

    public List<Number> getUncalled() {
        return numbers.stream().filter(n -> !n.isCalled()).collect(Collectors.toList());
    }

    public boolean allCalled() {
        return numbers.stream().allMatch(Number::isCalled);
    }
}
