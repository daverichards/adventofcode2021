package uk.daverichards.adventofcode2021.day25;

import lombok.Data;
import uk.daverichards.adventofcode2021.spatial.Vector2d;

@Data
public class SeaCucumber {
    public static final Vector2d EAST = new Vector2d(1, 0);
    public static final Vector2d SOUTH = new Vector2d(0, 1);

    private final Vector2d vector;
    private final char icon;

    public static SeaCucumber createEastFacing() {
        return new SeaCucumber(EAST, '>');
    }

    public static SeaCucumber createSouthFacing() {
        return new SeaCucumber(SOUTH, 'v');
    }
}
