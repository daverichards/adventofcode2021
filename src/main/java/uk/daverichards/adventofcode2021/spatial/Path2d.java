package uk.daverichards.adventofcode2021.spatial;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Path2d {
    private final List<Point2d> points;

    public Path2d addPoint(final Point2d point) {
        final List<Point2d> newPath = new ArrayList<>(points);
        newPath.add(point);
        return new Path2d(newPath);
    }

    public Point2d getLastPoint() {
        return points.get(points.size() - 1);
    }

    public boolean contains(final Point2d point) {
        return points.contains(point);
    }
}
