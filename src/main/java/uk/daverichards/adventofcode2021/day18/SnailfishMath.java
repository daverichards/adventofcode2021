package uk.daverichards.adventofcode2021.day18;

import lombok.Data;
import lombok.experimental.Accessors;

public class SnailfishMath {
    private static final int EXPLODE_THRESHOLD = 4;
    private static final int SPLIT_THRESHOLD = 9;
    public static SnailfishNumber add(final SnailfishNumber a, final SnailfishNumber b) {
        return reduce(new SnailfishNumberPair(a.copy(), b.copy()));
    }

    public static SnailfishNumber reduce(final SnailfishNumber n) {
        final SnailfishNumber reduced = n.copy();
        while (true) {
            // System.out.println(n);
            final ExplodeResult explodeResult = explode(reduced, 1);
            if (explodeResult.isExploded()) {
                continue;
            }
            if (split(reduced)) {
                continue;
            }
            break;
        }
        return reduced;
    }

    private static boolean split(final SnailfishNumber n) {
        if (n.isLiteral()) {
            // Literal (can't replace itself in the number)
            return false;
        }
        // Pair
        if (n.getPair().getLeft().isLiteral() && n.getPair().getLeft().getLiteral().getValue() > SPLIT_THRESHOLD) {
            // Split
            n.getPair().setLeft(generateSplitPair(n.getPair().getLeft().getLiteral()));
            return true;
        }
        if (split(n.getPair().getLeft())) {
            return true;
        }
        if (n.getPair().getRight().isLiteral() && n.getPair().getRight().getLiteral().getValue() > SPLIT_THRESHOLD) {
            // Split
            n.getPair().setRight(generateSplitPair(n.getPair().getRight().getLiteral()));
            return true;
        }
        if (split(n.getPair().getRight())) {
            return true;
        }
        return false;
    }

    private static SnailfishNumberPair generateSplitPair(SnailfishNumberLiteral literal) {
        final long val = literal.getValue();
        return new SnailfishNumberPair(
            new SnailfishNumberLiteral(val / 2),
            new SnailfishNumberLiteral((val / 2) + (val % 2))
        );
    }

    private static ExplodeResult explode(final SnailfishNumber n, final int level) {
        if (n.isLiteral()) {
            // Literal cannot be exploded
            return new ExplodeResult().setToExplode(false);
        }
        // Pair can be exploded
        if (level > EXPLODE_THRESHOLD && n.isExplodable()) {
            // Can be exploded
            return new ExplodeResult().setToExplode(true).setPair(n.getPair());
        }

        // Check left
        final ExplodeResult leftResult = explode(n.getPair().getLeft(), level + 1);
        if (leftResult.isToExplode()) {
            n.getPair().setLeft(new SnailfishNumberLiteral(0));
            leftResult.setExploded(true);
        }
        if (leftResult.isExploded()) {
            if (leftResult.carryRight > 0) {
                carryRight(n.getPair().getRight(), leftResult.carryRight);
            }
            return leftResult.setCarryRight(0);
        }

        // Check right
        final ExplodeResult rightResult = explode(n.getPair().getRight(), level + 1);
        if (rightResult.isToExplode()) {
            n.getPair().setRight(new SnailfishNumberLiteral(0));
            rightResult.setExploded(true);
        }
        if (rightResult.isExploded()) {
            if (rightResult.carryLeft > 0) {
                carryLeft(n.getPair().getLeft(), rightResult.carryLeft);
            }
            return rightResult.setCarryLeft(0);
        }

        return new ExplodeResult().setToExplode(false);
    }

    private static void carryLeft(final SnailfishNumber n, final long carry) {
        if (n.isLiteral()) {
            n.getLiteral().setValue(n.getLiteral().getValue() + carry);
            return;
        }
        carryLeft(n.getPair().getRight(), carry);
    }

    private static void carryRight(final SnailfishNumber n, final long carry) {
        if (n.isLiteral()) {
            n.getLiteral().setValue(n.getLiteral().getValue() + carry);
            return;
        }
        carryRight(n.getPair().getLeft(), carry);
    }

    @Data
    @Accessors(chain = true)
    private static class ExplodeResult {
        private boolean toExplode = false;
        private boolean exploded = false;
        private long carryLeft = 0;
        private long carryRight = 0;

        public ExplodeResult setToExplode(final boolean e) {
            toExplode = e;
            if (e) {
                exploded = false;
            }
            return this;
        }

        public ExplodeResult setExploded(final boolean e) {
            exploded = e;
            if (e) {
                toExplode = false;
            }
            return this;
        }

        public ExplodeResult setPair(SnailfishNumberPair pair) {
            if (!pair.isExplodable()) {
                throw new RuntimeException("Cannot set ExplodeResult as non-explodable Pair");
            }
            carryLeft = pair.getLeft().getLiteral().getValue();
            carryRight = pair.getRight().getLiteral().getValue();
            return this;
        }
    }
}
