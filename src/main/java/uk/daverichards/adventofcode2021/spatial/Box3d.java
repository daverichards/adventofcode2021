package uk.daverichards.adventofcode2021.spatial;

import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
public class Box3d {
    private final Point3d min;
    private final Point3d max;

    public boolean contains(final Point3d p) {
        return p.x >= min.x && p.x <= max.x
            && p.y >= min.y && p.y <= max.y
            && p.z >= min.z && p.z <= max.z;
    }

    public boolean intersects(final Box3d other) {
        return other.min.x <= max.x && other.max.x >= min.x
            && other.min.y <= max.y && other.max.y >= min.y
            && other.min.z <= max.z && other.max.z >= min.z;
    }

    public long size() {
        return (long) ((max.x - min.x) + 1)
            * (long) ((max.y - min.y) + 1)
            * (long) ((max.z - min.z) + 1);
    }

    public Set<Point3d> getPoints() {
        final Set<Point3d> points = new HashSet<>();
        for (int x = min.x; x <= max.x; x++) {
            for (int y = min.y; y <= max.y; y++) {
                for (int z = min.z; z <= max.z; z++) {
                    points.add(new Point3d(x, y, z));
                }
            }
        }
        return points;
    }
}
