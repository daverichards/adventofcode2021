package uk.daverichards.adventofcode2021.day20;

import lombok.Data;

@Data
public class Algorithm {
    private final String source;

    public char enhance(final String bin) {
        final int dec = Integer.parseInt(bin, 2);
        return source.charAt(dec);
    }
}
