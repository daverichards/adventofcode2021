package uk.daverichards.adventofcode2021.day17;

import uk.daverichards.adventofcode2021.spatial.Box2d;
import uk.daverichards.adventofcode2021.spatial.Map2d;
import uk.daverichards.adventofcode2021.spatial.Point2d;
import uk.daverichards.adventofcode2021.spatial.Vector2d;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Day17 {
    public static void main(String[] args) {
        try {
            System.out.println("Day 17");
//            final Box2d targetArea = new Box2d(20, -10, 30, -5);
            final Box2d targetArea = new Box2d(287, -76, 309, -48);
            final Point2d startingPoint = new Point2d(0, 0);
            
            System.out.println("Part 1:");
            final ProjectileResult highestResult = findHighestTrajectory(startingPoint, targetArea);
            System.out.printf("Highest point: %d\n\n", highestResult.getTrajectory().getPoints().stream().map(p -> p.y).reduce(0, Math::max));

            System.out.println("Part 2:");
            final Set<Vector2d> possibleVelocities = getAllPossibleVelocities(startingPoint, targetArea);
            System.out.printf("Number of possible velocities: %d\n\n", possibleVelocities.size());

        } catch (Exception e) {
            System.out.printf("ERROR: %s\n", e.getMessage());
            e.printStackTrace();
        }
    }

    private static int getMinXVelocity(final Point2d startingPoint, final Box2d targetArea) {
        // Get minimum number of steps to reach the target area, considering drag
        int inc = 0;
        for (int i = startingPoint.x; i < targetArea.getMin().x; i += inc) {
            inc++;
        }
        return inc;
    }

    private static int getMaxXVelocity(final Point2d startingPoint, final Box2d targetArea) {
        // Get maximum number of steps to reach the target area, considering drag
        return targetArea.getMax().x - startingPoint.x;
    }

    private static int getMinYVelocity(final Point2d startingPoint, final Box2d targetArea) {
        return targetArea.getMin().y - startingPoint.y;
    }

    private static ProjectileResult findHighestTrajectory(final Point2d startingPoint, final Box2d targetArea) {
        List<ProjectileResult> hits = new ArrayList<>();
        // minXVelocity is also the minimum number of steps to get to the target area
        final int minXVelocity = getMinXVelocity(startingPoint, targetArea);
        final int maxXVelocity = getMaxXVelocity(startingPoint, targetArea);
        Vector2d velocity = new Vector2d(minXVelocity, minXVelocity);

        final Vector2d reduceX = new Vector2d(-1, 0);
        final Vector2d increaseX = new Vector2d(1, 0);
        final Vector2d increaseY = new Vector2d(0, 1);

        for (int i = 0; i < 1000; i++) { // Is there a better way to know when we've reached the upper bound?
            ProjectileResult result = ProjectileSimulator.simulate(startingPoint, velocity, targetArea);

            if (result.isHit()) {
                hits.add(result);
                velocity = velocity.add(increaseY);
            }
            if (!result.isHit()) {
                Point2d missedPoint = result.getTrajectory().getLastPoint();
                if (missedPoint.x < targetArea.getMin().x && velocity.x < maxXVelocity) {
                    velocity = velocity.add(increaseX);
                } else if (missedPoint.x > targetArea.getMax().x && velocity.x > minXVelocity) {
                    velocity = velocity.add(reduceX);
                } else {
                    velocity = velocity.add(increaseY);
                }
            }
        }
        final int highestPoint = hits.stream().map(h -> h.getTrajectory().getPoints().stream().map(p -> p.y).reduce(0, Math::max)).reduce(0, Math::max);
        return hits.stream().filter(h -> h.getTrajectory().getPoints().stream().anyMatch(p -> p.y == highestPoint)).collect(Collectors.toList()).get(0);
    }

    private static Set<Vector2d> getAllPossibleVelocities(final Point2d startingPoint, final Box2d targetArea) {
        final int minXVelocity = getMinXVelocity(startingPoint, targetArea);
        final int maxXVelocity = getMaxXVelocity(startingPoint, targetArea);
        final int minYVelocity = getMinYVelocity(startingPoint, targetArea);
        final int maxYVelocity = findHighestTrajectory(startingPoint, targetArea).getInitialVelocity().y;

        List<ProjectileResult> hits = new ArrayList<>();

        for (int x = minXVelocity; x <= maxXVelocity; x++) {
            for (int y = minYVelocity; y <= maxYVelocity; y++) {
                ProjectileResult result = ProjectileSimulator.simulate(startingPoint, new Vector2d(x, y), targetArea);
                if (result.isHit()) {
                    hits.add(result);
                }
            }
        }

        return hits.stream().map(ProjectileResult::getInitialVelocity).collect(Collectors.toSet());
    }

    private static void printResult(final ProjectileResult result, final Box2d targetArea) {
        final Map2d<String> map = new Map2d<>();
        targetArea.getPoints().forEach(p -> map.setPoint(p, "T"));
        result.getTrajectory().getPoints().forEach(p -> map.setPoint(p, "#"));
        System.out.println(map.toString(s -> s, ".", true));
    }
}
