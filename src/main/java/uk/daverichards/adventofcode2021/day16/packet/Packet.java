package uk.daverichards.adventofcode2021.day16.packet;

public interface Packet {
    public Header getHeader();
    public int getVersionSum();
    public long getValue();
}
