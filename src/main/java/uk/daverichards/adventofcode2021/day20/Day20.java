package uk.daverichards.adventofcode2021.day20;

import uk.daverichards.adventofcode2021.spatial.Map2d;
import uk.daverichards.adventofcode2021.spatial.Point2d;
import uk.daverichards.adventofcode2021.util.FileReader;

import java.util.List;

public class Day20 {
  public static void main(String[] args) {
    try {
      System.out.println("Day 20");
      List<String> input = FileReader.loadStringList("input/day20.txt");

      final Algorithm algorithm = new Algorithm(input.get(0));
      final ImageEnhancer imageEnhancer = new ImageEnhancer(algorithm);

      final Map2d<Boolean> originalImage = new Map2d<>();
      for (int i = 2; i < input.size(); i++) {
        final String line = input.get(i);
        for (int j = 0; j < line.length(); j++) {
          originalImage.setPoint(new Point2d(j, i - 2), line.charAt(j) == '#');
        }
      }

      System.out.println("Part 1:");
      final Map2d<Boolean> enhanced2 = imageEnhancer.enhanceImage(originalImage, 2);
      final long pass2LitPixel = enhanced2.getPoints().values().stream().filter(b -> b).count();
      System.out.format("Number of lit pixels after 2 passes: %d\n\n", pass2LitPixel);

      System.out.println("Part 2:");
      final Map2d<Boolean> enhanced50 = imageEnhancer.enhanceImage(originalImage, 50);
      final long pass50LitPixel = enhanced50.getPoints().values().stream().filter(b -> b).count();
      System.out.format("Number of lit pixels after 50 passes: %d\n\n", pass50LitPixel);

    } catch (Exception e) {
      System.out.printf("ERROR: %s\n", e.getMessage());
      e.printStackTrace();
    }
  }
}
