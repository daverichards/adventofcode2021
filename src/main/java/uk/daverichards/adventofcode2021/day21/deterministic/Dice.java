package uk.daverichards.adventofcode2021.day21.deterministic;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Dice {
    private final int size;
    @Getter
    private int rollCounter = 0;

    public int roll() {
        rollCounter++;
        final int result = rollCounter % size;
        return result == 0 ? size : result;
    }
}
