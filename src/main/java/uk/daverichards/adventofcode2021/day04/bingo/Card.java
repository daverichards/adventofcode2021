package uk.daverichards.adventofcode2021.day04.bingo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Card {
    private final List<NumberSet> rows = new ArrayList<>();
    private final List<NumberSet> columns = new ArrayList<>();

    public void addRow(List<Number> numbers) {
        NumberSet row = new NumberSet();
        for (int i = 0; i < numbers.size(); i++) {
            row.addNumber(numbers.get(i));
            if (i >= columns.size()) {
                columns.add(new NumberSet());
            }
            columns.get(i).addNumber(numbers.get(i));
        }
        rows.add(row);
    }

    public boolean isWinner() {
        for (NumberSet ns : rows) {
            if (ns.allCalled()) {
                return true;
            }
        }
        for (NumberSet ns : columns) {
            if (ns.allCalled()) {
                return true;
            }
        }
        return false;
    }

    public List<Integer> getUncalledNumbers() {
        List<Number> uncalled = new ArrayList<>();
        rows.forEach(r -> uncalled.addAll(r.getUncalled()));
        return uncalled.stream().map(Number::getNumber).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Rows:\n");
        rows.forEach(r -> {
            r.getNumbers().forEach(sb::append);
            sb.append("\n");
        });
        sb.append("\nColumns:\n");
        columns.forEach(c -> {
            c.getNumbers().forEach(sb::append);
            sb.append("\n");
        });
        return sb.toString();
    }
}
