package uk.daverichards.adventofcode2021.day23;

import lombok.Data;
import uk.daverichards.adventofcode2021.spatial.Point2d;

@Data
public class Amphipod {
    private final char type;
    private final int energyCost;
    private final Point2d location;

    public Amphipod moveTo(final Point2d location) {
        return new Amphipod(type, energyCost, location);
    }
}
