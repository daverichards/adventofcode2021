package uk.daverichards.adventofcode2021.day02;

import uk.daverichards.adventofcode2021.util.FileReader;

import java.util.List;

public class Day02 {
    public static void main(String[] args) {
        try {
            System.out.println("Day 02");
            List<String> input = FileReader.loadStringList("input/day02.txt");

            int x = 0;
            int y = 0;

            for (String line : input) {
                if (line.startsWith("forward")) {
                    x += Integer.parseInt(line.substring(8));
                } else if (line.startsWith("up")) {
                    y -= Integer.parseInt(line.substring(3));
                } else if (line.startsWith("down")) {
                    y += Integer.parseInt(line.substring(5));
                }
            }

            System.out.println("Part 1:");
            System.out.printf("X: %d * Y: %d = %d\n\n", x, y, x * y);

            x = 0;
            y = 0;
            int a = 0;

            for (String line : input) {
                if (line.startsWith("forward")) {
                    int v = Integer.parseInt(line.substring(8));
                    x += v;
                    y += a * v;
                } else if (line.startsWith("up")) {
                    a -= Integer.parseInt(line.substring(3));
                } else if (line.startsWith("down")) {
                    a += Integer.parseInt(line.substring(5));
                }
            }

            System.out.println("Part 2:");
            System.out.printf("X: %d * Y: %d = %d\n\n", x, y, x * y);

        } catch (Exception e) {
            System.out.printf("ERROR: %s\n", e.getMessage());
        }
    }
}
