package uk.daverichards.adventofcode2021.util;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Utils {
    public static List<Character> toCharacters(String s) {
        return s.chars().mapToObj(c -> (char)c).collect(Collectors.toList());
    }

    public static List<Integer> toIntegers(String s) {
        return Arrays.stream(s.split("\\B")).map(Integer::valueOf).collect(Collectors.toList());
    }

    public static String padLeft(String s, int l, char c) {
        if (s.length() >= l) {
            return s;
        }
        StringBuilder sb = new StringBuilder();
        while (sb.length() < l - s.length()) {
            sb.append(c);
        }
        sb.append(s);
        return sb.toString();
    }
}
