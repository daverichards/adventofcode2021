package uk.daverichards.adventofcode2021.spatial;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
public class Line2d {
    private final Point2d a;
    private final Point2d b;

    public boolean isHorizontal() {
        return Objects.equals(a.y, b.y);
    }

    public boolean isVertical() {
        return Objects.equals(a.x, b.x);
    }

    public boolean isHorizontalOrVertical() {
        return isHorizontal() || isVertical();
    }
    public boolean isPoint() {
        return a == b;
    }

    public List<Point2d> getPoints() {
        if (isPoint()) {
            return List.of(a);
        }
        final List<Point2d> points = new ArrayList<>();
        final Vector2d v = getVector();
        final Vector2d pointVector = getPointVector();

        // We're stepping through the longest axis
        int iMax = Math.max(Math.abs(v.x), Math.abs(v.y));

        for (int i = 0; i <= iMax; i++) {
            points.add(a.add(pointVector.multiply(i)));
        }

        return points;
    }

    private Vector2d getVector() {
        return new Vector2d(b.x - a.x, b.y - a.y);
    }

    private Vector2d getPointVector() {
        // The smallest vector to map each point (Not always a unit vector)
        if (isPoint()) {
            return new Vector2d(0, 0);
        }
        final Vector2d v = getVector();
        if (v.x == 0) {
            return new Vector2d(0, v.y < 0 ? -1 : 1);
        }
        if (v.y == 0) {
            return new Vector2d(v.x < 0 ? -1 : 1, 0);
        }
        // Choose the longest side for the magnitude (this is why it's not a unit vector)
        float magnitude = Math.max(Math.abs(v.x), Math.abs(v.y));
        // Having to round to integers will potentially inaccuracy
        return new Vector2d(Math.round(v.x / magnitude), Math.round(v.y / magnitude));
    }

    @Override
    public String toString() {
        return "Line2d[" + a + " -> " + b + "]";
    }
}
