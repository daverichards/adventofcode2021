package uk.daverichards.adventofcode2021.day08;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DigitWiring {
    private final Map<Integer, List<Character>> digitPatterns = new HashMap<>();

    private final Map<Integer, Integer> uniqueLengthToDigit = Map.of(
            2, 1,
            3, 7,
            4, 4,
            7, 8
    );

    public void applySignalPatterns(final List<List<Character>> patterns) {
        // First assign unique digits
        patterns.forEach(p -> {
            if (uniqueLengthToDigit.containsKey(p.size())) {
                digitPatterns.put(uniqueLengthToDigit.get(p.size()), new ArrayList<>(p));
            }
        });

        if (digitPatterns.size() != uniqueLengthToDigit.size()) {
            throw new RuntimeException(String.format(
                    "Expected to have %d assigned unique digits, have %d (%s)",
                    uniqueLengthToDigit.size(),
                    digitPatterns.size(),
                    digitPatterns
            ));
        }

        // Remove these unique patterns now they're assigned
        patterns.removeIf(p -> uniqueLengthToDigit.containsKey(p.size()));

        // Now workout the remaining digits by intersecting the values with the known unique patterns
        patterns.forEach(p -> {
            if (p.size() == 5) {
                if (intersect(p, digitPatterns.get(1)).size() == 2) {
                    // 3
                    digitPatterns.put(3, new ArrayList<>(p));
                    return;
                }
                if (intersect(p, digitPatterns.get(4)).size() == 3) {
                    // 5
                    digitPatterns.put(5, new ArrayList<>(p));
                    return;
                }
                // 2
                digitPatterns.put(2, new ArrayList<>(p));
                return;
            }
            if (p.size() == 6) {
                if (intersect(p, digitPatterns.get(4)).size() == 4) {
                    // 9
                    digitPatterns.put(9, new ArrayList<>(p));
                    return;
                }
                if (intersect(p, digitPatterns.get(1)).size() == 2) {
                    // 0
                    digitPatterns.put(0, new ArrayList<>(p));
                    return;
                }
                // 6
                digitPatterns.put(6, new ArrayList<>(p));
                return;
            }
            throw new RuntimeException(String.format("Unexpected pattern length: %s", p));
        });

        // System.out.println(digitPatterns);
    }

    public int getDigitFromPattern(List<Character> pattern) {
        for (int i : digitPatterns.keySet()) {
            if (match(pattern, digitPatterns.get(i))) {
                return i;
            }
        }
        throw new RuntimeException(String.format("Could not for a digit for pattern %s", pattern));
    }

    private List<Character> intersect(List<Character> a, List<Character> b) {
        List<Character> result = new ArrayList<>();
        a.forEach(c -> {
            if (b.contains(c)) {
                result.add(c);
            }
        });
        return result;
    }

    private boolean match(List<Character> a, List<Character> b) {
        return a.size() == b.size() && intersect(a, b).size() == a.size();
    }
}
