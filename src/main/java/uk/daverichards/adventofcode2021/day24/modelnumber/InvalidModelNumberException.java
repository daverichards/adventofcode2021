package uk.daverichards.adventofcode2021.day24.modelnumber;

public class InvalidModelNumberException extends Exception {
    public InvalidModelNumberException(String message) {
        super(message);
    }
}
