package uk.daverichards.adventofcode2021.day21.deterministic;

import lombok.Getter;

public class Player {
    @Getter
    private int score = 0;

    public void incrementScore(final int inc) {
        score += inc;
    }
}
