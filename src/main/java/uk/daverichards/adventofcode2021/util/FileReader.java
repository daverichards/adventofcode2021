package uk.daverichards.adventofcode2021.util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class FileReader {
    public static List<String> loadStringList(String path) throws IOException, URISyntaxException {
        return Files.readAllLines(Paths.get(ClassLoader.getSystemResource(path).toURI()));
    }

    public static List<Integer> loadIntegerList(String path) throws IOException, URISyntaxException {
        return loadStringList(path).stream().map(Integer::valueOf).collect(Collectors.toList());
    }
}
