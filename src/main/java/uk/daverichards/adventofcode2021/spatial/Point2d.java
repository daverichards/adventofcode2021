package uk.daverichards.adventofcode2021.spatial;

import lombok.Data;

@Data
public class Point2d {
    public final Integer x;
    public final Integer y;

    public static Point2d fromString(String s) {
        String[] parts = s.trim().split(" *, *", 2);
        return new Point2d(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
    }

    public Point2d add(Vector2d v) {
        return new Point2d(x + v.x, y + v.y);
    }

    public Vector2d vectorTo(Point2d p) {
        return new Vector2d(p.x - x, p.y - y);
    }

    @Override
    public String toString() {
        return "Point2d[" + x + "," + y + "]";
    }
}
