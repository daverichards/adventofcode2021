package uk.daverichards.adventofcode2021.day08;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Entry {
    private final List<List<Character>> signalPatterns;
    private final List<List<Character>> outputDigits;

    private final DigitWiring digitWiring = new DigitWiring();

    private final List<Integer> uniqueDigitLengths = List.of(2, 3, 4, 7);

    public Entry(final String s) {
        String[] parts = s.split("\\|");
        this.signalPatterns = Arrays.stream(parts[0].trim().split(" +")).map(this::stringToChars).collect(Collectors.toList());
        this.outputDigits = Arrays.stream(parts[1].trim().split(" +")).map(this::stringToChars).collect(Collectors.toList());
    }

    public int frequencyOfUnitOutputDigits() {
        return (int) outputDigits.stream().filter(s -> uniqueDigitLengths.contains(s.size())).count();
    }

    public int getOutputNumber() {
        digitWiring.applySignalPatterns(signalPatterns);

        final StringBuilder sb = new StringBuilder();
        outputDigits.forEach(d -> {
            sb.append(digitWiring.getDigitFromPattern(d));
        });

        return Integer.parseInt(sb.toString(), 10);
    }

    private List<Character> stringToChars(final String s) {
        return s.chars().mapToObj(c -> (char)c).collect(Collectors.toList());
    }
}
