package uk.daverichards.adventofcode2021.day21;

import uk.daverichards.adventofcode2021.day21.deterministic.Dice;
import uk.daverichards.adventofcode2021.day21.deterministic.DeterministicGame;
import uk.daverichards.adventofcode2021.day21.deterministic.Player;
import uk.daverichards.adventofcode2021.day21.dirac.DiracGame;
import uk.daverichards.adventofcode2021.day21.dirac.Result;

public class Day21 {
  public static void main(String[] args) {
      try {
          System.out.println("Day 21");

          final int playerOneStartingPosition = 4;
          final int playerTwoStartingPosition = 7;

          final Dice detDice = new Dice(100);
          final DeterministicGame detGame = new DeterministicGame(detDice);

          final Player detPlayerOne = new Player();
          final Player detPlayerTwo = new Player();
          detGame.addPlayer(detPlayerOne, playerOneStartingPosition);
          detGame.addPlayer(detPlayerTwo, playerTwoStartingPosition);

          System.out.println("Part 1:");
          while (!detGame.isGameOver()) {
              detGame.playRound();
          }
          final int losingScore = Math.min(detPlayerOne.getScore(), detPlayerTwo.getScore());
          System.out.format("Losing Score (%d) * Dice rolls (%d) = %d\n\n", losingScore, detDice.getRollCounter(), losingScore * detDice.getRollCounter());

          System.out.println("Part 2:");
          final DiracGame diracGame = new DiracGame();
          final Result result = diracGame.play(playerOneStartingPosition, playerTwoStartingPosition);

          System.out.println(result);
          System.out.format("Most wins by a play: %d\n\n", Math.max(result.getPlayerOneWins(), result.getPlayerTwoWins()));


      } catch (Exception e) {
          System.out.printf("ERROR: %s\n", e.getMessage());
          e.printStackTrace();
      }
  }
}
