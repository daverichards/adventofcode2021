package uk.daverichards.adventofcode2021.day25;

import uk.daverichards.adventofcode2021.spatial.Map2d;
import uk.daverichards.adventofcode2021.spatial.Point2d;
import uk.daverichards.adventofcode2021.util.FileReader;

import java.util.List;

public class Day25 {
  public static void main(String[] args) {
      try {
          System.out.println("Day 25");
          final List<String> input = FileReader.loadStringList("input/day25.txt");

          final int mapHeight = input.size();
          final int mapWidth = input.get(0).length();

          final Map2d<SeaCucumber> map = new Map2d<>();

          for (int y = 0; y < mapHeight; y++) {
              for (int x = 0; x < mapWidth; x++) {
                  final char c = input.get(y).charAt(x);
                  if (c == '>') {
                      // EAST
                      map.setPoint(new Point2d(x, y), SeaCucumber.createEastFacing());
                  }
                  if (c == 'v') {
                      // SOUTH
                      map.setPoint(new Point2d(x, y), SeaCucumber.createSouthFacing());
                  }
              }
          }

          final SeaCucumberSimulator seaCucumberSimulator = new SeaCucumberSimulator(map, mapWidth, mapHeight);

          System.out.println("Part 1:");
          final int steps = seaCucumberSimulator.simulateUntilStationary();
          System.out.format("Number of steps before no sea cucumbers can move: %d\n\n", steps);

          System.out.println(seaCucumberSimulator.getMap().toString(seaCucumber -> Character.toString(seaCucumber.getIcon()), "."));

      } catch (Exception e) {
          System.out.printf("ERROR: %s\n", e.getMessage());
          e.printStackTrace();
      }
  }
}
