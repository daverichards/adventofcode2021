package uk.daverichards.adventofcode2021.day23;

import uk.daverichards.adventofcode2021.spatial.Point2d;
import uk.daverichards.adventofcode2021.util.FileReader;

import java.util.List;
import java.util.Map;

public class Day23 {
  public static void main(String[] args) {
    try {
      System.out.println("Day 23");

      System.out.println("Part 1:");
      // NOTE: Can take up to 100 mins
//      final AmphipodOrganiser amphipodOrganiser = createAmphipodOrganiser(FileReader.loadStringList("input/day23.txt"));
//      System.out.println(amphipodOrganiser.toString(amphipodOrganiser.getStaringState()));
//      final Path shortestPath = amphipodOrganiser.findShortestPath();
//      System.out.println(amphipodOrganiser.toString(shortestPath));
//      System.out.format("Shortest path energy spent: %d\n\n", shortestPath.getTotalEnergySpent());

      System.out.println("Part 2:");
      // NOTE: Can take up to 100 mins
      final AmphipodOrganiser amphipodOrganiser2 = createAmphipodOrganiser(FileReader.loadStringList("input/day23part2.txt"));
      System.out.println(amphipodOrganiser2.toString(amphipodOrganiser2.getStaringState()));
      final Path shortestPath2 = amphipodOrganiser2.findShortestPath();
      System.out.println(amphipodOrganiser2.toString(shortestPath2));
      System.out.format("Shortest path energy spent: %d\n\n", shortestPath2.getTotalEnergySpent());

    } catch (Exception e) {
      System.out.printf("ERROR: %s\n", e.getMessage());
      e.printStackTrace();
    }
  }

  private static AmphipodOrganiser createAmphipodOrganiser(List<String> input) {
    // Map hardcoded within organiser
    final AmphipodOrganiser amphipodOrganiser = new AmphipodOrganiser(input.size() - 3);

    final Map<Character, Integer> amphipodEnergyMap = Map.of(
        'A', 1,
        'B', 10,
        'C', 100,
        'D', 1000
    );

    for (int y = 0; y < input.size(); y++) {
      final String line = input.get(y);
      for (int x = 0; x < line.length(); x++) {
        final char c = line.charAt(x);
        if (amphipodEnergyMap.containsKey(c)) {
          amphipodOrganiser.addAmphipod(new Amphipod(c, amphipodEnergyMap.get(c), new Point2d(x - 1, y - 1)));
        }
      }
    }
    return amphipodOrganiser;
  }
}
