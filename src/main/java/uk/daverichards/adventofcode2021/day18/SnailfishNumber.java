package uk.daverichards.adventofcode2021.day18;

public interface SnailfishNumber {
    SnailfishNumber copy();
    boolean isLiteral();
    SnailfishNumberLiteral getLiteral();
    boolean isPair();
    SnailfishNumberPair getPair();
    boolean isExplodable();
    long getMagnitude();
}
