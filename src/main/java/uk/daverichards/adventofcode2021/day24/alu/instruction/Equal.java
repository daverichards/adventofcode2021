package uk.daverichards.adventofcode2021.day24.alu.instruction;

import uk.daverichards.adventofcode2021.day24.alu.InputBuffer;
import uk.daverichards.adventofcode2021.day24.alu.State;

import java.util.Objects;

public class Equal implements Instruction {
    private final String addr;
    private final long addLiteral;
    private final String addAddr;

    public Equal(final String addr, final long literal) {
        this.addr = addr;
        addLiteral = literal;
        addAddr = null;
    }

    public Equal(final String addr, final String otherAddr) {
        this.addr = addr;
        addLiteral = 0;
        addAddr = otherAddr;
    }

    @Override
    public State execute(State state, InputBuffer inputBuffer) {
        if (addAddr == null) {
            return state.set(addr, state.get(addr) == addLiteral ? 1L : 0);
        }
        return state.set(addr, Objects.equals(state.get(addr), state.get(addAddr)) ? 1L : 0);
    }
}
