package uk.daverichards.adventofcode2021.day14;

import lombok.Data;

@Data
public class Pair {
    final Character a;
    final Character b;

    public static Pair fromString(final String s) {
        if (s.length() != 2) {
            throw new RuntimeException(String.format("Invalid pair: %s", s));
        }
        final char[] chars = s.toCharArray();
        return new Pair(chars[0], chars[1]);
    }
}
