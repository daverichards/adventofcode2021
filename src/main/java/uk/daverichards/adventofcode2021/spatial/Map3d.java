package uk.daverichards.adventofcode2021.spatial;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

public class Map3d<T> {
    private final Map<Point3d, T> points = new HashMap<>();

    @Getter
    private Point3d minBound;
    @Getter
    private Point3d maxBound;

    public void setPoint(final Point3d p, final T v) {
        points.put(p, v);
        expandBounds(p);
    }

    public T getPoint(Point3d p) {
        return points.get(p);
    }

    public T getPointOrDefault(Point3d p, T dflt) {
        return points.getOrDefault(p, dflt);
    }

    public Map<Point3d, T> getPoints() {
        return new HashMap<>(points);
    }

    private void expandBounds(Point3d p) {
        minBound = minBound == null ? p : new Point3d(Math.min(minBound.x, p.x), Math.min(minBound.y, p.y), Math.min(minBound.z, p.z));
        maxBound = maxBound == null ? p : new Point3d(Math.max(maxBound.x, p.x), Math.max(maxBound.y, p.y), Math.max(maxBound.z, p.z));
    }
}
