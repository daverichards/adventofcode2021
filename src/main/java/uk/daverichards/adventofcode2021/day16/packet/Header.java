package uk.daverichards.adventofcode2021.day16.packet;

import lombok.Data;

@Data
public class Header {
    private final int version;
    private final int typeId;

    public boolean isLiteral() {
        return typeId == 4;
    }

    public boolean isOperator() {
        return typeId != 4;
    }
}
