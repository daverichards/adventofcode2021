package uk.daverichards.adventofcode2021.day08;

import uk.daverichards.adventofcode2021.util.FileReader;

import java.util.ArrayList;
import java.util.List;

public class Day08 {
    public static void main(String[] args) {
        try {
            System.out.println("Day 08");
            List<String> input = FileReader.loadStringList("input/day08.txt");

            final List<Entry> entries = new ArrayList<>();
            for (String i : input) {
                entries.add(new Entry(i));
            }

            System.out.println("Part 1:");
            final int frequencyOfUniqueDigits = entries.stream().map(Entry::frequencyOfUnitOutputDigits).reduce(0, Integer::sum);
            System.out.printf("Number of unique digits: %d\n\n", frequencyOfUniqueDigits);

            System.out.println("Part 2:");
            final int sumOfOutputValues = entries.stream().map(Entry::getOutputNumber).reduce(0, Integer::sum);
            System.out.printf("Sum of output values: %d\n\n", sumOfOutputValues);

        } catch (Exception e) {
            System.out.printf("ERROR: %s\n", e.getMessage());
            e.printStackTrace();
        }
    }
}
