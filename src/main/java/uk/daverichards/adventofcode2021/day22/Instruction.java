package uk.daverichards.adventofcode2021.day22;

import lombok.Value;
import uk.daverichards.adventofcode2021.spatial.Box3d;

@Value
public class Instruction {
    boolean on;
    Box3d box;
}
