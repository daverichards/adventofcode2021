package uk.daverichards.adventofcode2021.day12;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class PathFinder {
    private final Map<String, Set<String>> cavePaths = new HashMap<>();

    public void addTwoWayPath(final String a, final String b) {
        if (isBigCave(a) && isBigCave(b)) {
            throw new RuntimeException(String.format("2 big caves can't be connect, got '%s' and '%s'.", a, b));
        }
        addOneWayPath(a, b);
        addOneWayPath(b, a);
    }

    private void addOneWayPath(final String from, final String to) {
        if (!cavePaths.containsKey(from)) {
            cavePaths.put(from, new HashSet<>());
        }
        cavePaths.get(from).add(to);
    }

    private boolean isSmallCave(final String cave) {
        return cave.equals(cave.toLowerCase());
    }

    private boolean isBigCave(final String cave) {
        return cave.equals(cave.toUpperCase());
    }

    private boolean isStartCave(final String cave) {
        return cave.equalsIgnoreCase("start");
    }

    private boolean isEndCave(final String cave) {
        return cave.equalsIgnoreCase("end");
    }

    public Set<List<String>> findPaths() {
        return findPaths(List.of("start"));
    }

    public Set<List<String>> findPaths(int smallCaveRevisitLimit) {
        return findPaths(List.of("start"), smallCaveRevisitLimit);
    }

    private Set<List<String>> findPaths(List<String> currentPath) {
        return findPaths(currentPath, 0);
    }

    private Set<List<String>> findPaths(List<String> currentPath, int smallCaveRevisitLimit) {
        final String cave = currentPath.get(currentPath.size() - 1);
        if (!cavePaths.containsKey(cave)) {
            throw new RuntimeException(String.format("Unable to find cave '%s'.", cave));
        }
        final int smallCavesRevisited = smallCavesRevisitedInPath(currentPath);
        final Set<List<String>> nextPaths = new HashSet<>();
        cavePaths.get(cave).forEach(next -> {
            if (isStartCave(next)) {
                // Cannot go back to the start
                return;
            }
            if (isSmallCave(next) && currentPath.contains(next) && smallCavesRevisited >= smallCaveRevisitLimit) {
                // Already visited this small cave
                return;
            }
            final List<String> nextPath = new ArrayList<>(currentPath);
            nextPath.add(next);
            if (isEndCave(next)) {
                // Next is the end, add path and return
                nextPaths.add(nextPath);
                return;
            }
            nextPaths.addAll(findPaths(nextPath, smallCaveRevisitLimit));
        });
        return nextPaths;
    }

    private int smallCavesRevisitedInPath(List<String> path) {
        final List<String> smallCaves = path.stream().filter(this::isSmallCave).collect(Collectors.toList());
        final Set<String> uniqueSmallCaves = new HashSet<>(smallCaves);
        return smallCaves.size() - uniqueSmallCaves.size();
    }
}
